//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.payroll;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.contact.EmployeeLink;
import java.util.ArrayList;

/**
* Describes the Timesheet resource
*/
public class Timesheet   
{
    /**
    * The Employee
    */
    private EmployeeLink Employee;
    public EmployeeLink getEmployee() {
        return Employee;
    }

    public void setEmployee(EmployeeLink value) {
        Employee = value;
    }

    /**
    * Timesheet Start Date
    */
    private java.util.Date StartDate = new java.util.Date();
    public java.util.Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(java.util.Date value) {
        StartDate = value;
    }

    /**
    * Timesheet End Date
    */
    private java.util.Date EndDate = new java.util.Date();
    public java.util.Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(java.util.Date value) {
        EndDate = value;
    }

    /**
    * Timesheet Lines
    */
    private ArrayList<TimesheetLine> Lines = new ArrayList<TimesheetLine>();
    public ArrayList<TimesheetLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<TimesheetLine> value) {
        Lines = value;
    }

    /**
    * Timesheet String
    */
    private String URI;
    public String getURI() {
        return URI;
    }

    public void setString(String value) {
        URI = value;
    }

}


