//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.sale.CalculateDiscountsFeesSale;

/**
* Describe the Sale/CustomerPayment/CalculateDiscountsFees request's item
*/
public class CalculateDiscountsFeesItem   
{
    /**
    * Unique id to allow you to identify the discount calculated item
    */
    private int RequestID;
    public int getRequestID() {
        return RequestID;
    }

    public void setRequestID(int value) {
        RequestID = value;
    }

    /**
    * Sale to use for discount calculation
    */
    private CalculateDiscountsFeesSale Sale;
    public CalculateDiscountsFeesSale getSale() {
        return Sale;
    }

    public void setSale(CalculateDiscountsFeesSale value) {
        Sale = value;
    }

    /**
    * Payment date
    */
    private java.util.Date PaymentDate = new java.util.Date();
    public java.util.Date getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(java.util.Date value) {
        PaymentDate = value;
    }

}


