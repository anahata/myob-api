//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.company;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
* Describes the company file system preferences
*/
public class CompanySystemPreferences   
{
    /**
    * Off - turn off the category trackingOnAndNotRequired - turn on the category tracking categories are not required on All TransactionsOnAndRequired - turn on the category tracking categories are required on All Transactions
    */
    private CategoryTracking categoryTracking = CategoryTracking.Off;
    public CategoryTracking getCategoryTracking() {
        return categoryTracking;
    }

    public void setCategoryTracking(CategoryTracking value) {
        categoryTracking = value;
    }

    /**
    * Transactions CAN'T be Changed; They Must be Reversed
    */
    private boolean TransactionsCannotBeChangedMustBeReversed;
    public boolean getTransactionsCannotBeChangedMustBeReversed() {
        return TransactionsCannotBeChangedMustBeReversed;
    }

    public void setTransactionsCannotBeChangedMustBeReversed(boolean value) {
        TransactionsCannotBeChangedMustBeReversed = value;
    }

    /**
    * Lock Period
    * Null - Unlock period for allow entriesNot null - Period locked and disallow entries prior to [value]
    */
    private java.util.Date LockPeriodPriorTo = new java.util.Date();
    public java.util.Date getLockPeriodPriorTo() {
        return LockPeriodPriorTo;
    }

    public void setLockPeriodPriorTo(java.util.Date value) {
        LockPeriodPriorTo = value;
    }

}


