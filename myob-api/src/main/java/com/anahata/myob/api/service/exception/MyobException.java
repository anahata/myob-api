/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.service.exception;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import lombok.AllArgsConstructor;
import org.apache.http.HttpStatus;

/**
 *
 * @author pablo
 */
@AllArgsConstructor
public class MyobException extends RuntimeException {

    int httpCode;

    String message;

    /**
     * Creates a new instance of <code>MyobServiceException</code> without detail message.
     *
     * @param t
     */
    public MyobException(Throwable t) {
        super(t);
    }

    public MyobException(String message) {
        super(message);
    }

    public MyobException(String message, Throwable t) {
        super(message, t);
    }

    public MyobException(int httpCode, String message, Throwable cause) {
        super(message + " statusCode " + httpCode, cause);
        this.httpCode = httpCode;
        this.message = message;
    }

    public boolean isConflict() {
        return httpCode == HttpStatus.SC_CONFLICT;
    }

}
