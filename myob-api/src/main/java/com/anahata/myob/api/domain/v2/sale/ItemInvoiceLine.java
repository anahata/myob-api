//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.inventory.ItemLink;
import com.anahata.myob.api.domain.v2.sale.InvoiceLine;

/**
* Describe the Sale/Invoice/Item's Lines
*/
public class ItemInvoiceLine  extends InvoiceLine 
{
    /**
    * The quantity of goods shipped.
    */
    private java.math.BigDecimal ShipQuantity;
    public java.math.BigDecimal getShipQuantity() {
        return ShipQuantity;
    }

    public void setShipQuantity(java.math.BigDecimal value) {
        ShipQuantity = value;
    }

    /**
    * Unit price assigned to the item.
    */
    private java.math.BigDecimal UnitPrice;
    public java.math.BigDecimal getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(java.math.BigDecimal value) {
        UnitPrice = value;
    }

    /**
    * Discount rate applicable to the line of the sale invoice.
    */
    private java.math.BigDecimal DiscountPercent;
    public java.math.BigDecimal getDiscountPercent() {
        return DiscountPercent;
    }

    public void setDiscountPercent(java.math.BigDecimal value) {
        DiscountPercent = value;
    }

    /**
    * Dollar amount posted to the Asset account setup on an item using 'I Inventory'
    */
    private java.math.BigDecimal CostOfGoodsSold;
    public java.math.BigDecimal getCostOfGoodsSold() {
        return CostOfGoodsSold;
    }

    public void setCostOfGoodsSold(java.math.BigDecimal value) {
        CostOfGoodsSold = value;
    }

    /**
    * Item for the invoice line
    */
    private ItemLink Item;
    public ItemLink getItem() {
        return Item;
    }

    public void setItem(ItemLink value) {
        Item = value;
    }

}


