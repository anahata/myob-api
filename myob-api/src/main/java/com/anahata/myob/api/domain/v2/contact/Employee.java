//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.contact.Contact;
import com.anahata.myob.api.domain.v2.contact.EmployeePaymentDetailsLink;
import com.anahata.myob.api.domain.v2.contact.EmployeePayrollDetailsLink;
import com.anahata.myob.api.domain.v2.contact.EmployeeStandardPayLink;

/**
* Describes an Employee resource
*/
public class Employee  extends Contact 
{
    /**
    * The location of the 
    *  {@link #EmployeePayrollDetails}
    *  resource for this 
    *  {@link #Employee}
    * 
    * Only applicable for AU AccountRight (Plus and above) where Payroll has been configured
    */
    private EmployeePayrollDetailsLink EmployeePayrollDetails;
    public EmployeePayrollDetailsLink getEmployeePayrollDetails() {
        return EmployeePayrollDetails;
    }

    public void setEmployeePayrollDetails(EmployeePayrollDetailsLink value) {
        EmployeePayrollDetails = value;
    }

    /**
    * The location of the 
    *  {@link #EmployeePaymentDetails}
    *  resource for this 
    *  {@link #Employee}
    * 
    * Only applicable for 2014.3 cloud and 2014.4 desktop where supported
    */
    private EmployeePaymentDetailsLink EmployeePaymentDetails;
    public EmployeePaymentDetailsLink getEmployeePaymentDetails() {
        return EmployeePaymentDetails;
    }

    public void setEmployeePaymentDetails(EmployeePaymentDetailsLink value) {
        EmployeePaymentDetails = value;
    }

    /**
    * The location of the 
    *  {@link #EmployeeStandardPay}
    *  resource for this 
    *  {@link #Employee}
    * 
    * Only applicable for 2014.4 cloud and 2014.5 desktop where supported
    */
    private EmployeeStandardPayLink EmployeeStandardPay;
    public EmployeeStandardPayLink getEmployeeStandardPay() {
        return EmployeeStandardPay;
    }

    public void setEmployeeStandardPay(EmployeeStandardPayLink value) {
        EmployeeStandardPay = value;
    }

}


