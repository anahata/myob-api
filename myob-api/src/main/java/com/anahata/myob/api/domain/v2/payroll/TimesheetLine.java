//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.payroll;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.contact.CustomerLink;
import com.anahata.myob.api.domain.v2.generalledger.JobLink;
import com.anahata.myob.api.domain.v2.payrollCategory.ActivityLink;
import com.anahata.myob.api.domain.v2.payrollCategory.PayrollCategoryLink;
import java.util.ArrayList;

/**
* Describes the TimesheetLine
*/
public class TimesheetLine   
{
    /**
    * The PayrollCategory
    */
    private PayrollCategoryLink PayrollCategory;
    public PayrollCategoryLink getPayrollCategory() {
        return PayrollCategory;
    }

    public void setPayrollCategory(PayrollCategoryLink value) {
        PayrollCategory = value;
    }

    /**
    * The Job
    */
    private JobLink Job;
    public JobLink getJob() {
        return Job;
    }

    public void setJob(JobLink value) {
        Job = value;
    }

    /**
    * The Activity
    */
    private ActivityLink Activity;
    public ActivityLink getActivity() {
        return Activity;
    }

    public void setActivity(ActivityLink value) {
        Activity = value;
    }

    /**
    * The Customer
    */
    private CustomerLink Customer;
    public CustomerLink getCustomer() {
        return Customer;
    }

    public void setCustomer(CustomerLink value) {
        Customer = value;
    }

    /**
    * The Notes
    */
    private String Notes;
    public String getNotes() {
        return Notes;
    }

    public void setNotes(String value) {
        Notes = value;
    }

    /**
    * The day entries
    */
    private ArrayList<TimesheetEntry> Entries = new ArrayList<TimesheetEntry>();
    public ArrayList<TimesheetEntry> getEntries() {
        return Entries;
    }

    public void setEntries(ArrayList<TimesheetEntry> value) {
        Entries = value;
    }

}


