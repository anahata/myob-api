//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.payrollCategory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.math.BigDecimal;

/**
* The calculation basis details for 
*  {@link #PayrollCategoryEntitlement}
*/
public class PayrollCategoryEntitlementCalculationBasis   
{
    /**
    * The type of calculation basis
    */
    private PayrollCategoryEntitlementCalculationBasisType Type = PayrollCategoryEntitlementCalculationBasisType.UserEntered;
    public PayrollCategoryEntitlementCalculationBasisType getType() {
        return Type;
    }

    public void setType(PayrollCategoryEntitlementCalculationBasisType value) {
        Type = value;
    }

    /**
    * The percentage of the PayrollCategory, used when 
    *  {@link #Type}
    *  is 
    *  {@link #PayrollCategoryEntitlementCalculationBasisType.PercentageOfPayrollCategory}
    */
    private BigDecimal PercentageOf;
    public BigDecimal getPercentageOf() {
        return PercentageOf;
    }

    public void setPercentageOf(BigDecimal value) {
        PercentageOf = value;
    }

    /**
    * The 
    *  {@link #PayrollCategory}
    *  to take a percentage of, used when 
    *  {@link #Type}
    *  is 
    *  {@link #PayrollCategoryEntitlementCalculationBasisType.PercentageOfPayrollCategory}
    */
    private PayrollCategoryLink PayrollCategory;
    public PayrollCategoryLink getPayrollCategory() {
        return PayrollCategory;
    }

    public void setPayrollCategory(PayrollCategoryLink value) {
        PayrollCategory = value;
    }

    /**
    * The number of fixed hours to accrue per 
    *  {@link #AccrualPeriod}
    * , used when 
    *  {@link #Type}
    *  is 
    *  {@link #PayrollCategoryEntitlementCalculationBasisType.FixedHours}
    */
    private BigDecimal FixedHoursOf;
    public BigDecimal getFixedHoursOf() {
        return FixedHoursOf;
    }

    public void setFixedHoursOf(BigDecimal value) {
        FixedHoursOf = value;
    }

    /**
    * The Accrual Period that the 
    *  {@link #FixedHoursOf}
    *  is based, used when 
    *  {@link #Type}
    *  is 
    *  {@link #PayrollCategoryEntitlementCalculationBasisType.FixedHours}
    */
    private PayrollCategoryEntitlementAccrualPeriod AccrualPeriod = PayrollCategoryEntitlementAccrualPeriod.PayPeriod;
    public PayrollCategoryEntitlementAccrualPeriod getAccrualPeriod() {
        return AccrualPeriod;
    }

    public void setAccrualPeriod(PayrollCategoryEntitlementAccrualPeriod value) {
        AccrualPeriod = value;
    }

}


