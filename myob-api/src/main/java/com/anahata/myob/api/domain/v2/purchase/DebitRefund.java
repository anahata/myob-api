//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.purchase;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.SupplierLink;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.sale.DepositTo;

/**
* Describe the Purchase/DebitRefund resource
*/
public class DebitRefund  extends BaseEntity 
{
    /**
    * Use the supplied account or use the undeposited funds account
    */
    private DepositTo depositTo = DepositTo.Account;
    public DepositTo getDepositTo() {
        return depositTo;
    }

    public void setDepositTo(DepositTo value) {
        depositTo = value;
    }

    /**
    * The account to use
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * The bill associated with the refund
    */
    private BillLink Bill;
    public BillLink getBill() {
        return Bill;
    }

    public void setBill(BillLink value) {
        Bill = value;
    }

    /**
    * The supplier
    */
    private SupplierLink Supplier;
    public SupplierLink getSupplier() {
        return Supplier;
    }

    public void setSupplier(SupplierLink value) {
        Supplier = value;
    }

    /**
    * The refund number
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * The date of the refund
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * The amount of the refund
    */
    private java.math.BigDecimal Amount;
    public java.math.BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(java.math.BigDecimal value) {
        Amount = value;
    }

    /**
    * Additional information
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * The payment method
    */
    private String PaymentMethod;
    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String value) {
        PaymentMethod = value;
    }

}


