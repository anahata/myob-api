/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template fileString, choose Tools | Templates
 * and open the template in the editor.
 */

package com.anahata.myob.api;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.myob.api.auth.DataFileCredentials;
import com.anahata.myob.api.auth.MyobPlugin;
import com.anahata.myob.api.auth.OAuthAccessToken;
import com.anahata.myob.api.auth.OauthAuthenticator;
import java.io.Serializable;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
/**
 *
 * @author pablo
 */
@Setter
@Getter
public class MyobEndPoint implements Serializable {
    public static final String CLOUD_ENDPOINT = "https://api.myob.com/accountright/";
    
    private final String uri;
    
    private DataFileCredentials credentials;

    private OAuthAccessToken oauthAccessToken;
    
    public MyobEndPoint(@NonNull String uri) {
        this.uri = uri;
        this.credentials = new DataFileCredentials();
    }

    public MyobEndPoint(@NonNull String uri, DataFileCredentials credentials) {
        this.uri = uri;
        this.credentials = credentials;
    }

    public MyobEndPoint(@NonNull String uri, DataFileCredentials credentials, @NonNull OAuthAccessToken oauthCredentials) {
        this.uri = uri;
        this.credentials = credentials;
        this.oauthAccessToken = oauthCredentials;
    }
    
    public synchronized void refreshOAuthToken() throws Exception {
        oauthAccessToken = OauthAuthenticator.refreshAccessToken(oauthAccessToken);
    }
    
    public MyobPlugin plugin() {
        return oauthAccessToken != null ? oauthAccessToken.getPlugin() : null;
    }

    @Override
    public String toString() {
        return "MyobEndPoint{" + "\nuri=" + uri + "\ncredentials=" + credentials + "\noauthAccessToken=" + oauthAccessToken + '}';
    }
    
    
}
