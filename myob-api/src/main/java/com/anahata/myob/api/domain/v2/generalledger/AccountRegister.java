//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;

/**
* Describes the Account Register resource
*/
public class AccountRegister  extends BaseEntity 
{
    /**
    * Number of the Account (Read Only)
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * Year
    */
    private int Year;
    public int getYear() {
        return Year;
    }

    public void setYear(int value) {
        Year = value;
    }

    /**
    * Month
    */
    private int Month;
    public int getMonth() {
        return Month;
    }

    public void setMonth(int value) {
        Month = value;
    }

    /**
    * Activity
    */
    private java.math.BigDecimal Activity;
    public java.math.BigDecimal getActivity() {
        return Activity;
    }

    public void setActivity(java.math.BigDecimal value) {
        Activity = value;
    }

    /**
    * Adjustment
    */
    private java.math.BigDecimal Adjustment;
    public java.math.BigDecimal getAdjustment() {
        return Adjustment;
    }

    public void setAdjustment(java.math.BigDecimal value) {
        Adjustment = value;
    }

    /**
    * Activity13Period
    */
    private java.math.BigDecimal YearEndActivity;
    public java.math.BigDecimal getYearEndActivity() {
        return YearEndActivity;
    }

    public void setYearEndActivity(java.math.BigDecimal value) {
        YearEndActivity = value;
    }

    /**
    * Adjustment13Period
    */
    private java.math.BigDecimal YearEndAdjustment;
    public java.math.BigDecimal getYearEndAdjustment() {
        return YearEndAdjustment;
    }

    public void setYearEndAdjustment(java.math.BigDecimal value) {
        YearEndAdjustment = value;
    }

}


