//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.payrollCategory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.math.BigDecimal;

/**
* The extra details for Hourly 
*  {@link #WageType}
*/
public class HourlyDetails   
{
    /**
    * The type of adjustment
    */
    private HourlyDetailsPayRate PayRate = HourlyDetailsPayRate.RegularRate;
    public HourlyDetailsPayRate getPayRate() {
        return PayRate;
    }

    public void setPayRate(HourlyDetailsPayRate value) {
        PayRate = value;
    }

    /**
    * The multiplier when dealing with a change to regular rate
    */
    private BigDecimal RegularRateMultiplier;
    public BigDecimal getRegularRateMultiplier() {
        return RegularRateMultiplier;
    }

    public void setRegularRateMultiplier(BigDecimal value) {
        RegularRateMultiplier = value;
    }

    /**
    * The alternative fixed-rate
    */
    private BigDecimal FixedHourlyRate;
    public BigDecimal getFixedHourlyRate() {
        return FixedHourlyRate;
    }

    public void setFixedHourlyRate(BigDecimal value) {
        FixedHourlyRate = value;
    }

    /**
    * True, if this category used for paid leave entitlements
    */
    private boolean AutomaticallyAdjustBaseAmounts;
    public boolean getAutomaticallyAdjustBaseAmounts() {
        return AutomaticallyAdjustBaseAmounts;
    }

    public void setAutomaticallyAdjustBaseAmounts(boolean value) {
        AutomaticallyAdjustBaseAmounts = value;
    }

}


