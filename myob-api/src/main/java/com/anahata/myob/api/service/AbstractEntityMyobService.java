/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.service;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.EntityPage;
import com.anahata.myob.api.service.exception.MyobException;
import com.anahata.util.lang.builder.DelimitedStringBuilder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import static com.anahata.myob.api.service.AbstractMyobService.*;

/**
 *
 * @author pablo
 * @param <T>
 * @param <U>
 */
@Slf4j
public abstract class AbstractEntityMyobService<T extends EntityPage<U>, U extends BaseEntity> extends AbstractMyobService {

    protected final Class<T> pageType;

    private final Class<U> entityType;

    protected AbstractEntityMyobService(String context, Class<T> pageType, Class<U> entityType) {
        super(context);
        this.pageType = pageType;
        this.entityType = entityType;
    }

    /**
     * Finds all records for the given entity, performing pagination as required.
     *
     * @return
     * @throws MyobException
     */
    public List<U> findAll() throws MyobException {
        String json = sendReceive(null, null, null, null);
        T page = fromJson(json, pageType);
        List<U> ret = new ArrayList<>();
        //ret.addAll(page.getItems());
        Number count = page.getCount();
        if (count.intValue() <= 400) {
            ret.addAll(page.getItems());
        }
        while (!page.isLast()) {
            json = sendReceive(null, "$top=" + count, null, null);
            page = fromJson(json, pageType);
            ret.addAll(page.getItems());
        }

        return ret;
    }

    public U find(String uid) throws MyobException {
        String ret = sendReceive(uid, null, null, null);
        U all = fromJson(ret, entityType);
        return all;
    }

    public U update(U item) throws MyobException {
        String ret = sendReceive(item.getUID(), null, "PUT", toJson(item));
        item = fromJson(ret, entityType);
        return item;
    }

    public U create(U item) throws MyobException {
        String ret = sendReceive(null, null, "POST", toJson(item));
        return fromJson(ret, entityType);
    }

    public U remove(String uid) throws MyobException {
        String ret = sendReceive(uid, null, "DELETE", null);
        U all = fromJson(ret, entityType);
        return all;
    }

    public List<U> find(String filterExpression, String orderBy, Integer top, Integer skip) throws MyobException {
        List<U> ret = new ArrayList<>();
        DelimitedStringBuilder dsb = new DelimitedStringBuilder("&");
        if (filterExpression != null) {
            dsb.append("$filter=" + URLEncoder.encode(filterExpression));
        }
        if (orderBy != null) {
            dsb.append("$orderBy=" + URLEncoder.encode(orderBy));
        }
        if (top != null) {
            dsb.append("$top=" + top);
        }
        if (skip != null) {
            dsb.append("$skip=" + skip);
        }
        String json = sendReceive(null, dsb.toString(), null, null);
        T page = fromJson(json, pageType);
        ret.addAll(page.getItems());
        return ret;
    }

}
