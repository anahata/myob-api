//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
* Describes an AccountingProperties resource
*/
public class AccountingProperties   
{
    /**
    * Starting date for the company file. Conversion date must always be for the first day of the selected month. You can't record transactions dated earlier than the conversion date.
    */
    private java.util.Date ConversionDate = new java.util.Date();
    public java.util.Date getConversionDate() {
        return ConversionDate;
    }

    public void setConversionDate(java.util.Date value) {
        ConversionDate = value;
    }

    /**
    * Year that the current financial year ends. For example, 2014 indicates that the financial year ends in 2014.
    */
    private int CurrentFinancialYear;
    public int getCurrentFinancialYear() {
        return CurrentFinancialYear;
    }

    public void setCurrentFinancialYear(int value) {
        CurrentFinancialYear = value;
    }

    /**
    * Number representing the last month of the financial year. For example, 3 indicates March.
    */
    private int LastMonthFinancialYear;
    public int getLastMonthFinancialYear() {
        return LastMonthFinancialYear;
    }

    public void setLastMonthFinancialYear(int value) {
        LastMonthFinancialYear = value;
    }

}


