//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.inventory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.contact.SupplierLink;
import java.math.BigDecimal;

/**
* Describes the restocking information
*/
public class RestockingInformation   
{
    /**
    * The minimum level to generate a restocking alert
    */
    private BigDecimal MinimumLevelForRestockingAlert;
    public BigDecimal getMinimumLevelForRestockingAlert() {
        return MinimumLevelForRestockingAlert;
    }

    public void setMinimumLevelForRestockingAlert(BigDecimal value) {
        MinimumLevelForRestockingAlert = value;
    }

    /**
    * The main supplier of the item
    */
    private SupplierLink Supplier;
    public SupplierLink getSupplier() {
        return Supplier;
    }

    public void setSupplier(SupplierLink value) {
        Supplier = value;
    }

    /**
    * The supplier's item number
    */
    private String SupplierItemNumber;
    public String getSupplierItemNumber() {
        return SupplierItemNumber;
    }

    public void setSupplierItemNumber(String value) {
        SupplierItemNumber = value;
    }

    /**
    * The default number of items/units to but when re-ordering
    */
    private java.math.BigDecimal DefaultOrderQuantity;
    public java.math.BigDecimal getDefaultOrderQuantity() {
        return DefaultOrderQuantity;
    }

    public void setDefaultOrderQuantity(java.math.BigDecimal value) {
        DefaultOrderQuantity = value;
    }

}


