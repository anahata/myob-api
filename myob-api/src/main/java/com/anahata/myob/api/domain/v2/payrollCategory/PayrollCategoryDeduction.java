//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.payrollCategory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import java.util.ArrayList;

/**
* Describes the deduction PayrollCategory
*/
public class PayrollCategoryDeduction  extends PayrollCategory 
{
    /**
    * The account that is affected
    */
    private AccountLink PayableAccount;
    public AccountLink getPayableAccount() {
        return PayableAccount;
    }

    public void setPayableAccount(AccountLink value) {
        PayableAccount = value;
    }

    /**
    * Details about the calculation
    */
    private PayrollCategoryDeductionCalculationBasis CalculationBasis;
    public PayrollCategoryDeductionCalculationBasis getCalculationBasis() {
        return CalculationBasis;
    }

    public void setCalculationBasis(PayrollCategoryDeductionCalculationBasis value) {
        CalculationBasis = value;
    }

    /**
    * Details about the limit
    */
    private PayrollCategoryDeductionLimit Limit;
    public PayrollCategoryDeductionLimit getLimit() {
        return Limit;
    }

    public void setLimit(PayrollCategoryDeductionLimit value) {
        Limit = value;
    }

    /**
    * The 
    *  {@link #PayrollCategory}
    *  that are excluded dStringng calculations
    */
    private ArrayList<PayrollCategoryLink> Exemptions = new ArrayList<PayrollCategoryLink>();
    public ArrayList<PayrollCategoryLink> getExemptions() {
        return Exemptions;
    }

    public void setExemptions(ArrayList<PayrollCategoryLink> value) {
        Exemptions = value;
    }

}


