//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.banking;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.CardLink;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.CategoryLink;
import com.anahata.myob.api.domain.v2.purchase.PayFrom;
import com.anahata.myob.api.domain.v2.sale.DocumentAction;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
* A spend money transaction entity
*/
public class SpendMoneyTxn  extends BaseEntity 
{
    /**
    * If allocating a banking account for the payment specify Account.
    * If using electronic payments specify ElectronicPayments
    */
    @SerializedName("PayFrom") 
    private PayFrom payFrom = PayFrom.Account;
    public PayFrom getPayFrom() {
        return payFrom;
    }

    public void setPayFrom(PayFrom value) {
        payFrom = value;
    }

    /**
    * The account to pay to
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * The Contact (Card) associated with the transaction
    */
    private CardLink Contact;
    public CardLink getContact() {
        return Contact;
    }

    public void setContact(CardLink value) {
        Contact = value;
    }

    /**
    * Payee address of the Contact.
    */
    private String PayeeAddress;
    public String getPayeeAddress() {
        return PayeeAddress;
    }

    public void setPayeeAddress(String value) {
        PayeeAddress = value;
    }

    /**
    * ONLY applicable for Electronic Payments.
    * Particulars attached to electronic payment.
    */
    private String StatementParticulars;
    public String getStatementParticulars() {
        return StatementParticulars;
    }

    public void setStatementParticulars(String value) {
        StatementParticulars = value;
    }

    /**
    * Statement Code (New Zealand only)
    */
    private String StatementCode;
    public String getStatementCode() {
        return StatementCode;
    }

    public void setStatementCode(String value) {
        StatementCode = value;
    }

    /**
    * Statement Reference (New Zealand only)
    */
    private String StatementReference;
    public String getStatementReference() {
        return StatementReference;
    }

    public void setStatementReference(String value) {
        StatementReference = value;
    }

    /**
    * Transaction Payment Number
    */
    private String PaymentNumber;
    public String getPaymentNumber() {
        return PaymentNumber;
    }

    public void setPaymentNumber(String value) {
        PaymentNumber = value;
    }

    /**
    * Transaction date entry, format YYYY-MM-DD HH:MM:SS
    * { 'Date': '2013-08-11 13:33:02' }
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * The Total Amount Paid
    * Read-only
    */
    private java.math.BigDecimal AmountPaid;
    public java.math.BigDecimal getAmountPaid() {
        return AmountPaid;
    }

    public void setAmountPaid(java.math.BigDecimal value) {
        AmountPaid = value;
    }

    /**
    * True indicates the transaction is set to tax inclusive.False indicates the transaction is not tax inclusive.
    */
    private boolean IsTaxInclusive;
    public boolean getIsTaxInclusive() {
        return IsTaxInclusive;
    }

    public void setIsTaxInclusive(boolean value) {
        IsTaxInclusive = value;
    }

    /**
    * Total of all tax amounts applicable to the transaction.
    */
    private java.math.BigDecimal TotalTax;
    public java.math.BigDecimal getTotalTax() {
        return TotalTax;
    }

    public void setTotalTax(java.math.BigDecimal value) {
        TotalTax = value;
    }

    /**
    * Memo text describing the transaction.
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * Transaction Lines
    */
    private ArrayList<SpendMoneyTxnLine> Lines = new ArrayList<SpendMoneyTxnLine>();
    public ArrayList<SpendMoneyTxnLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<SpendMoneyTxnLine> value) {
        Lines = value;
    }

    /**
    * Was a cheque printed
    */
    private boolean ChequePrinted;
    public boolean getChequePrinted() {
        return ChequePrinted;
    }

    public void setChequePrinted(boolean value) {
        ChequePrinted = value;
    }

    /**
    * Delivery status assigned to payment:
    * ToBePrintedToBeEmailedToBePrintedAndEmailedAlreadyPrintedOrSent
    */
    private DocumentAction DeliveryStatus = DocumentAction.Nothing;
    public DocumentAction getDeliveryStatus() {
        return DeliveryStatus;
    }

    public void setDeliveryStatus(DocumentAction value) {
        DeliveryStatus = value;
    }

    /**
    * The Category associated with the transaction
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

}


