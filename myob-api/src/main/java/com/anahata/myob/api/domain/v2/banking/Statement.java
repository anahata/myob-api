//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.banking;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.SourceTransaction;
import java.util.ArrayList;

/**
* Bank Statement
*/
public class Statement  extends BaseEntity 
{
    /**
    * Date
    */
    private java.util.Date StatementDate = new java.util.Date();
    public java.util.Date getStatementDate() {
        return StatementDate;
    }

    public void setStatementDate(java.util.Date value) {
        StatementDate = value;
    }

    /**
    * Date
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * Description
    */
    private String Description;
    public String getDescription() {
        return Description;
    }

    public void setDescription(String value) {
        Description = value;
    }

    /**
    * Account
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * Amount
    */
    private java.math.BigDecimal Amount;
    public java.math.BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(java.math.BigDecimal value) {
        Amount = value;
    }

    /**
    * Is the transaction a Credit or a Debit
    */
    private boolean IsCredit;
    public boolean getIsCredit() {
        return IsCredit;
    }

    public void setIsCredit(boolean value) {
        IsCredit = value;
    }

    /**
    * Source transactions
    */
    private ArrayList<SourceTransaction> SourceTransactions = new ArrayList<SourceTransaction>();
    public ArrayList<SourceTransaction> getSourceTransactions() {
        return SourceTransactions;
    }

    public void setSourceTransactions(ArrayList<SourceTransaction> value) {
        SourceTransactions = value;
    }

    /**
    * Status
    */
    private StatementStatus Status = StatementStatus.Uncoded;
    public StatementStatus getStatus() {
        return Status;
    }

    public void setStatus(StatementStatus value) {
        Status = value;
    }

}


