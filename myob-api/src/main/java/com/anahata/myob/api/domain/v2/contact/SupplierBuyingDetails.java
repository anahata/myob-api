//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import com.anahata.myob.api.domain.v2.sale.DocumentAction;
import java.io.Serializable;

/**
* Describes the Supplier Buying Details
*/
public class SupplierBuyingDetails   implements Serializable
{
    /**
    * The default purchase layout
    */
    private PurchaseLayoutType PurchaseLayout = PurchaseLayoutType.NoDefault;
    public PurchaseLayoutType getPurchaseLayout() {
        return PurchaseLayout;
    }

    public void setPurchaseLayout(PurchaseLayoutType value) {
        PurchaseLayout = value;
    }

    /**
    * Named form selected as default printed form.
    */
    private String PrintedForm;
    public String getPrintedForm() {
        return PrintedForm;
    }

    public void setPrintedForm(String value) {
        PrintedForm = value;
    }

    /**
    * Default supplier delivery status.
    */
    private DocumentAction PurchaseOrderDelivery = DocumentAction.Nothing;
    public DocumentAction getPurchaseOrderDelivery() {
        return PurchaseOrderDelivery;
    }

    public void setPurchaseOrderDelivery(DocumentAction value) {
        PurchaseOrderDelivery = value;
    }

    /**
    * A link to the expense account resource
    */
    private AccountLink ExpenseAccount;
    public AccountLink getExpenseAccount() {
        return ExpenseAccount;
    }

    public void setExpenseAccount(AccountLink value) {
        ExpenseAccount = value;
    }

    /**
    * Default payment memo.
    */
    private String PaymentMemo;
    public String getPaymentMemo() {
        return PaymentMemo;
    }

    public void setPaymentMemo(String value) {
        PaymentMemo = value;
    }

    /**
    * Default selected purchase comment.
    */
    private String PurchaseComment;
    public String getPurchaseComment() {
        return PurchaseComment;
    }

    public void setPurchaseComment(String value) {
        PurchaseComment = value;
    }

    /**
    * The suppliers hourly billing rate exclusive of tax.
    */
    private java.math.BigDecimal SupplierBillingRate;
    public java.math.BigDecimal getSupplierBillingRate() {
        return SupplierBillingRate;
    }

    public void setSupplierBillingRate(java.math.BigDecimal value) {
        SupplierBillingRate = value;
    }

    /**
    * Shipping method text.
    */
    private String ShippingMethod;
    public String getShippingMethod() {
        return ShippingMethod;
    }

    public void setShippingMethod(String value) {
        ShippingMethod = value;
    }

    /**
    * Indicates the supplier contact is setup for reportable taxable payments. (AU only)
    */
    private boolean IsReportable;
    public boolean getIsReportable() {
        return IsReportable;
    }

    public void setIsReportable(boolean value) {
        IsReportable = value;
    }

    /**
    * Cost per hour of providing the suppliers services when generating an activity slip.
    */
    private java.math.BigDecimal CostPerHour;
    public java.math.BigDecimal getCostPerHour() {
        return CostPerHour;
    }

    public void setCostPerHour(java.math.BigDecimal value) {
        CostPerHour = value;
    }

    /**
    * The supplier credit limit
    */
    private SupplierCredit Credit;
    public SupplierCredit getCredit() {
        return Credit;
    }

    public void setCredit(SupplierCredit value) {
        Credit = value;
    }

    /**
    * ABN Number (Must be 11 digits and formatted as XX XXX XXX XXX).
    */
    private String ABN;
    public String getABN() {
        return ABN;
    }

    public void setABN(String value) {
        ABN = value;
    }

    /**
    * ABN branch number.
    */
    private String ABNBranch;
    public String getABNBranch() {
        return ABNBranch;
    }

    public void setABNBranch(String value) {
        ABNBranch = value;
    }

    /**
    * Tax id number.
    */
    private String TaxIdNumber;
    public String getTaxIdNumber() {
        return TaxIdNumber;
    }

    public void setTaxIdNumber(String value) {
        TaxIdNumber = value;
    }

    /**
    * A link to a tax code resource
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

    /**
    * A link to a Tax code resource for use with freight charges
    */
    private TaxCodeLink FreightTaxCode;
    public TaxCodeLink getFreightTaxCode() {
        return FreightTaxCode;
    }

    public void setFreightTaxCode(TaxCodeLink value) {
        FreightTaxCode = value;
    }

    /**
    * Indicates to use the supplier tax code.
    */
    private boolean UseSupplierTaxCode;
    public boolean getUseSupplierTaxCode() {
        return UseSupplierTaxCode;
    }

    public void setUseSupplierTaxCode(boolean value) {
        UseSupplierTaxCode = value;
    }

    /**
    * The supplier terms
    */
    private SupplierTerms Terms;
    public SupplierTerms getTerms() {
        return Terms;
    }

    public void setTerms(SupplierTerms value) {
        Terms = value;
    }

}


