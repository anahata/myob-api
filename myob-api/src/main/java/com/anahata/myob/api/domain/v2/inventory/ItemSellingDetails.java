//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.inventory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import java.math.BigDecimal;

/**
* The details when 
*  {@link #Item.IsSold}
*  is true
*/
public class ItemSellingDetails   
{
    /**
    * The base selling price
    */
    private java.math.BigDecimal BaseSellingPrice;
    public java.math.BigDecimal getBaseSellingPrice() {
        return BaseSellingPrice;
    }

    public void setBaseSellingPrice(java.math.BigDecimal value) {
        BaseSellingPrice = value;
    }

    /**
    * The unit of measure when selling items
    */
    private String SellingUnitOfMeasure;
    public String getSellingUnitOfMeasure() {
        return SellingUnitOfMeasure;
    }

    public void setSellingUnitOfMeasure(String value) {
        SellingUnitOfMeasure = value;
    }

    /**
    * The number of items in a selling unit
    */
    private BigDecimal ItemsPerSellingUnit;
    public BigDecimal getItemsPerSellingUnit() {
        return ItemsPerSellingUnit;
    }

    public void setItemsPerSellingUnit(BigDecimal value) {
        ItemsPerSellingUnit = value;
    }

    /**
    * The tax code to use when selling items
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

    /**
    * Is tax inclusive of the price
    */
    private boolean IsTaxInclusive;
    public boolean getIsTaxInclusive() {
        return IsTaxInclusive;
    }

    public void setIsTaxInclusive(boolean value) {
        IsTaxInclusive = value;
    }

    /**
    * The  price value to use to calculate the Sales Tax (AU only)
    */
    private CalculateSalesTaxOn calculateSalesTaxOn = CalculateSalesTaxOn.ActualSellingPrice;
    public CalculateSalesTaxOn getCalculateSalesTaxOn() {
        return calculateSalesTaxOn;
    }

    public void setCalculateSalesTaxOn(CalculateSalesTaxOn value) {
        calculateSalesTaxOn = value;
    }

    /**
    * A link to the item's selling price matrix
    */
    private String PriceMatrixURI;
    public String getPriceMatrixString() {
        return PriceMatrixURI;
    }

    public void setPriceMatrixURI(String value) {
        PriceMatrixURI = value;
    }

}


