//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.purchase;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.contact.SupplierLink;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.sale.DocumentAction;
import java.util.ArrayList;

/**
* Describe the SupplierPayment/RecordWithDiscountsAndFees resource
*/
public class SupplierPaymentRecordWithDiscountsAndFees   
{
    /**
    * The UID of the SupplierPayment (not yet supported)
    */
    private String UID;
    public String getUID() {
        return UID;
    }

    public void setUID(String value) {
        UID = value;
    }

    /**
    * If allocating a banking account for the payment specify Account.
    * If using electronic payments specify ElectronicPayments
    */
    private PayFrom payFrom = PayFrom.Account;
    public PayFrom getPayFrom() {
        return payFrom;
    }

    public void setPayFrom(PayFrom value) {
        payFrom = value;
    }

    /**
    * The account to pay to (only need UID)
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * Pay to supplier
    */
    private SupplierLink Supplier;
    public SupplierLink getSupplier() {
        return Supplier;
    }

    public void setSupplier(SupplierLink value) {
        Supplier = value;
    }

    /**
    * Payee address of the purchase bill.
    */
    private String PayeeAddress;
    public String getPayeeAddress() {
        return PayeeAddress;
    }

    public void setPayeeAddress(String value) {
        PayeeAddress = value;
    }

    /**
    * ONLY applicable for Electronic Payments.
    * Particulars attached to electronic payment.
    */
    private String StatementParticulars;
    public String getStatementParticulars() {
        return StatementParticulars;
    }

    public void setStatementParticulars(String value) {
        StatementParticulars = value;
    }

    /**
    * ONLY APPLICABLE FOR NZ REGION.
    * Code attached to electronic payment.
    */
    private String StatementCode;
    public String getStatementCode() {
        return StatementCode;
    }

    public void setStatementCode(String value) {
        StatementCode = value;
    }

    /**
    * ONLY APPLICABLE FOR NZ REGION.
    * Reference attached to electronic payment.
    */
    private String StatementReference;
    public String getStatementReference() {
        return StatementReference;
    }

    public void setStatementReference(String value) {
        StatementReference = value;
    }

    /**
    * ID No of payment transaction
    */
    private String PaymentNumber;
    public String getPaymentNumber() {
        return PaymentNumber;
    }

    public void setPaymentNumber(String value) {
        PaymentNumber = value;
    }

    /**
    * Transaction date entry, format YYYY-MM-DD HH:MM:SS
    * { 'Date': '2013-08-11 13:33:02' }
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * Total of all amounts paid to the purchase bill/bills.
    */
    private java.math.BigDecimal FinanceCharge;
    public java.math.BigDecimal getFinanceCharge() {
        return FinanceCharge;
    }

    public void setFinanceCharge(java.math.BigDecimal value) {
        FinanceCharge = value;
    }

    /**
    * Memo text of the supplier payment entry.
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * Payment to the purchases
    */
    private ArrayList<SupplierPaymentRecordWithDiscountsAndFeesLine> Lines = new ArrayList<SupplierPaymentRecordWithDiscountsAndFeesLine>();
    public ArrayList<SupplierPaymentRecordWithDiscountsAndFeesLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<SupplierPaymentRecordWithDiscountsAndFeesLine> value) {
        Lines = value;
    }

    /**
    * Delivery status assigned to payment:
    * ToBePrintedToBeEmailedToBePrintedAndEmailedAlreadyPrintedOrSent
    */
    private DocumentAction DeliveryStatus = DocumentAction.Nothing;
    public DocumentAction getDeliveryStatus() {
        return DeliveryStatus;
    }

    public void setDeliveryStatus(DocumentAction value) {
        DeliveryStatus = value;
    }

}


