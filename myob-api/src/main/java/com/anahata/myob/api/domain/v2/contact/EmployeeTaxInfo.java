//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.payrollCategory.PayrollCategoryLink;
import com.anahata.myob.api.domain.v2.payrollCategory.TaxTableLink;

/**
* EmployeeTaxInfo
*/
public class EmployeeTaxInfo   
{
    /**
    * Tax File Number
    */
    private String TaxFileNumber;
    public String getTaxFileNumber() {
        return TaxFileNumber;
    }

    public void setTaxFileNumber(String value) {
        TaxFileNumber = value;
    }

    /**
    * Tax Table
    */
    private TaxTableLink TaxTable;
    public TaxTableLink getTaxTable() {
        return TaxTable;
    }

    public void setTaxTable(TaxTableLink value) {
        TaxTable = value;
    }

    /**
    * Withholding Variation Rate
    */
    private java.math.BigDecimal WithholdingVariationRate;
    public java.math.BigDecimal getWithholdingVariationRate() {
        return WithholdingVariationRate;
    }

    public void setWithholdingVariationRate(java.math.BigDecimal value) {
        WithholdingVariationRate = value;
    }

    /**
    * Total Rebates Per Year
    */
    private java.math.BigDecimal TotalRebatesPerYear;
    public java.math.BigDecimal getTotalRebatesPerYear() {
        return TotalRebatesPerYear;
    }

    public void setTotalRebatesPerYear(java.math.BigDecimal value) {
        TotalRebatesPerYear = value;
    }

    /**
    * Extra Tax Per Pay
    */
    private java.math.BigDecimal ExtraTaxPerPay;
    public java.math.BigDecimal getExtraTaxPerPay() {
        return ExtraTaxPerPay;
    }

    public void setExtraTaxPerPay(java.math.BigDecimal value) {
        ExtraTaxPerPay = value;
    }

    /**
    * Tax Category
    */
    private PayrollCategoryLink TaxCategory;
    public PayrollCategoryLink getTaxCategory() {
        return TaxCategory;
    }

    public void setTaxCategory(PayrollCategoryLink value) {
        TaxCategory = value;
    }

}


