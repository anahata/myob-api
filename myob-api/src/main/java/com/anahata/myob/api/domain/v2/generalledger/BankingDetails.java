//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
* Banking Details
*/
public class BankingDetails   
{
    /**
    * BSB as provided by the financial institution.
    */
    private String BSBNumber;
    public String getBSBNumber() {
        return BSBNumber;
    }

    public void setBSBNumber(String value) {
        BSBNumber = value;
    }

    /**
    * Account number as provided by the financial institution.
    * 
    * For Australia: formatted as BankAccountNumber e.g. 
    * For New Zealand: formatted as BankCode-BankAccountNumber-Suffix e.g.
    */
    private String BankAccountNumber;
    public String getBankAccountNumber() {
        return BankAccountNumber;
    }

    public void setBankAccountNumber(String value) {
        BankAccountNumber = value;
    }

    /**
    * Bank account name.
    */
    private String BankAccountName;
    public String getBankAccountName() {
        return BankAccountName;
    }

    public void setBankAccountName(String value) {
        BankAccountName = value;
    }

    /**
    * Company trading name if applicable for bank account
    */
    private String CompanyTradingName;
    public String getCompanyTradingName() {
        return CompanyTradingName;
    }

    public void setCompanyTradingName(String value) {
        CompanyTradingName = value;
    }

    /**
    * Bank code as provided by the financial institution.
    */
    private String BankCode;
    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String value) {
        BankCode = value;
    }

    /**
    * True indicates the bank account will be used to create bank files (ABA). 
    * False indicates the bank account will not be used to create bank files.
    */
    private boolean CreateBankFiles;
    public boolean getCreateBankFiles() {
        return CreateBankFiles;
    }

    public void setCreateBankFiles(boolean value) {
        CreateBankFiles = value;
    }

    /**
    * Direct entry user id as provided by the financial institution.
    */
    private String DirectEntryUserId;
    public String getDirectEntryUserId() {
        return DirectEntryUserId;
    }

    public void setDirectEntryUserId(String value) {
        DirectEntryUserId = value;
    }

    /**
    * True indicates the bank account requires a self balancing transaction. 
    * False indicates the bank account does not require a self balancing transaction.
    */
    private boolean IncludeSelfBalancingTransaction;
    public boolean getIncludeSelfBalancingTransaction() {
        return IncludeSelfBalancingTransaction;
    }

    public void setIncludeSelfBalancingTransaction(boolean value) {
        IncludeSelfBalancingTransaction = value;
    }

    /**
    * Statement Code (New Zealand only)
    */
    private String StatementCode;
    public String getStatementCode() {
        return StatementCode;
    }

    public void setStatementCode(String value) {
        StatementCode = value;
    }

    /**
    * Statement Reference (New Zealand only)
    */
    private String StatementReference;
    public String getStatementReference() {
        return StatementReference;
    }

    public void setStatementReference(String value) {
        StatementReference = value;
    }

    /**
    * Statement Particulars (Statement Text - Australia, Particulars - New Zealand)
    */
    private String StatementParticulars;
    public String getStatementParticulars() {
        return StatementParticulars;
    }

    public void setStatementParticulars(String value) {
        StatementParticulars = value;
    }

}


