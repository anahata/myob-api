//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



public enum ProductLevelType
{
    /**
    * The AccountRight product level that the company file supports
    * 
    * Basic
    */
    dummyEnum0,
    dummyEnum1,
    dummyEnum2,
    dummyEnum3,
    dummyEnum4,
    dummyEnum5,
    dummyEnum6,
    dummyEnum7,
    dummyEnum8,
    dummyEnum9,
    Basic,
    dummyEnum10,
    dummyEnum11,
    dummyEnum12,
    dummyEnum13,
    dummyEnum14,
    dummyEnum15,
    dummyEnum16,
    dummyEnum17,
    dummyEnum18,
    /**
    * Standard
    */
    Standard,
    dummyEnum19,
    dummyEnum20,
    dummyEnum21,
    dummyEnum22,
    dummyEnum23,
    dummyEnum24,
    dummyEnum25,
    dummyEnum26,
    dummyEnum27,
    /**
    * Plus
    */
    Plus,
    dummyEnum28,
    dummyEnum29,
    dummyEnum30,
    dummyEnum31,
    dummyEnum32,
    dummyEnum33,
    dummyEnum34,
    dummyEnum35,
    dummyEnum36,
    dummyEnum37,
    dummyEnum38,
    dummyEnum39,
    dummyEnum40,
    dummyEnum41,
    dummyEnum42,
    dummyEnum43,
    dummyEnum44,
    dummyEnum45,
    dummyEnum46,
    /**
    * Premier
    */
    Premier
}

