//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.payrollCategory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import java.util.ArrayList;

/**
* Describes the Wage PayrollCategory
*/
public class PayrollCategoryWage  extends PayrollCategory 
{
    /**
    * The wage type
    */
    private WageType wageType = WageType.Hourly;
    public WageType getWageType() {
        return wageType;
    }

    public void setWageType(WageType value) {
        wageType = value;
    }

    /**
    * The overridden wages expense account
    */
    private AccountLink OverriddenWagesExpenseAccount;
    public AccountLink getOverriddenWagesExpenseAccount() {
        return OverriddenWagesExpenseAccount;
    }

    public void setOverriddenWagesExpenseAccount(AccountLink value) {
        OverriddenWagesExpenseAccount = value;
    }

    /**
    * Extra details when dealing with a Hourly 
    *  {@link #WageType}
    */
    private HourlyDetails HourlyDetails;
    public HourlyDetails getHourlyDetails() {
        return HourlyDetails;
    }

    public void setHourlyDetails(HourlyDetails value) {
        HourlyDetails = value;
    }

    /**
    * Exempt this 
    *  {@link #PayrollCategory}
    *  from deduction and tax calculations
    */
    private ArrayList<PayrollCategoryLink> Exemptions = new ArrayList<PayrollCategoryLink>();
    public ArrayList<PayrollCategoryLink> getExemptions() {
        return Exemptions;
    }

    public void setExemptions(ArrayList<PayrollCategoryLink> value) {
        Exemptions = value;
    }

}


