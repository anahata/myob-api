//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.timebilling;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.timebilling.ActivityStatus;
import com.anahata.myob.api.domain.v2.timebilling.ActivityType;
import com.anahata.myob.api.domain.v2.timebilling.ChargeableDetails;

/**
* Describes the base Activity
*/
public class Activity  extends BaseEntity 
{
    /**
    * Construct an 
    *  {@link #Activity}
    *  object
    */
    public Activity() throws Exception {
        setIsActive(true);
    }

    /**
    * The display identifier
    */
    private String DisplayID;
    public String getDisplayID() {
        return DisplayID;
    }

    public void setDisplayID(String value) {
        DisplayID = value;
    }

    /**
    * The name of the 
    *  {@link #Activity}
    */
    private String Name;
    public String getName() {
        return Name;
    }

    public void setName(String value) {
        Name = value;
    }

    /**
    * The activity description
    */
    private String Description;
    public String getDescription() {
        return Description;
    }

    public void setDescription(String value) {
        Description = value;
    }

    /**
    * The active state
    */
    private boolean IsActive;
    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean value) {
        IsActive = value;
    }

    /**
    * The activity type
    */
    private ActivityType Type = ActivityType.Hourly;
    public ActivityType getType() {
        return Type;
    }

    public void setType(ActivityType value) {
        Type = value;
    }

    /**
    * The unit of measurement; if 
    *  {@link #Type}
    *  is 
    *  {@link #ActivityType.Hourly}
    *  then this defaults to "Hour"
    */
    private String UnitOfMeasurement;
    public String getUnitOfMeasurement() {
        return UnitOfMeasurement;
    }

    public void setUnitOfMeasurement(String value) {
        UnitOfMeasurement = value;
    }

    /**
    * The activity status
    */
    private ActivityStatus Status = ActivityStatus.Chargeable;
    public ActivityStatus getStatus() {
        return Status;
    }

    public void setStatus(ActivityStatus value) {
        Status = value;
    }

    /**
    * Describes how the activity is charged
    */
    private ChargeableDetails ChargeableDetails;
    public ChargeableDetails getChargeableDetails() {
        return ChargeableDetails;
    }

    public void setChargeableDetails(ChargeableDetails value) {
        ChargeableDetails = value;
    }

}


