//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.JobLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import com.anahata.myob.api.domain.v2.sale.InvoiceLineType;

/**
* Common class for sale bill line
*/
public class InvoiceLine   
{
    /**
    * Sequence of the entry within the item sale set.
    * ONLY required for updating an existing item sale line.NOT required when creating a new item sale invoice.
    */
    private int RowID;
    public int getRowID() {
        return RowID;
    }

    public void setRowID(int value) {
        RowID = value;
    }

    /**
    * Invoice line type, can consist of the following:
    * TransactionHeaderSubTotal
    */
    private InvoiceLineType Type = InvoiceLineType.Transaction;
    public InvoiceLineType getType() {
        return Type;
    }

    public void setType(InvoiceLineType value) {
        Type = value;
    }

    /**
    * Description text for the sale line.
    */
    private String Description;
    public String getDescription() {
        return Description;
    }

    public void setDescription(String value) {
        Description = value;
    }

    /**
    * Total amount for the line only.
    */
    private java.math.BigDecimal Total;
    public java.math.BigDecimal getTotal() {
        return Total;
    }

    public void setTotal(java.math.BigDecimal value) {
        Total = value;
    }

    /**
    * Job for the line
    */
    private JobLink Job;
    public JobLink getJob() {
        return Job;
    }

    public void setJob(JobLink value) {
        Job = value;
    }

    /**
    * TaxCode for the line
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

    /**
    * Incrementing number that can be used for change control but does does not preserve a date or a time.
    * ONLY required for updating an existing sale line.NOT required when creating a new sale invoice.
    */
    private String RowVersion;
    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String value) {
        RowVersion = value;
    }

}


