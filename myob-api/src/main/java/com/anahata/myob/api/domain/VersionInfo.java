//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;

/**
* The deployed API version info
*/
public class VersionInfo   
{
    /**
    * The build number of the deployed API
    */
    private String Build;
    public String getBuild() {
        return Build;
    }

    public void setBuild(String value) {
        Build = value;
    }

    /**
    * A list of available resources supported by this API
    */
    private ArrayList<VersionMap> Resources = new ArrayList<VersionMap>();
    public ArrayList<VersionMap> getResources() {
        return Resources;
    }

    public void setResources(ArrayList<VersionMap> value) {
        Resources = value;
    }

}


