//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.generalledger.CategoryLink;

/**
* Describes the Employee Standard Pay
*/
public class EmployeeStandardPay  extends BaseEntity 
{
    /**
    * Employee
    */
    private EmployeeLink Employee;
    public EmployeeLink getEmployee() {
        return Employee;
    }

    public void setEmployee(EmployeeLink value) {
        Employee = value;
    }

    /**
    * Employee Payroll Details
    */
    private EmployeePayrollDetailsLink EmployeePayrollDetails;
    public EmployeePayrollDetailsLink getEmployeePayrollDetails() {
        return EmployeePayrollDetails;
    }

    public void setEmployeePayrollDetails(EmployeePayrollDetailsLink value) {
        EmployeePayrollDetails = value;
    }

    /**
    * PayFrequency
    */
    private PayFrequency payFrequency = PayFrequency.Weekly;
    public PayFrequency getPayFrequency() {
        return payFrequency;
    }

    public void setPayFrequency(PayFrequency value) {
        payFrequency = value;
    }

    /**
    * Hours per pay frequency
    */
    private java.math.BigDecimal HoursPerPayFrequency;
    public java.math.BigDecimal getHoursPerPayFrequency() {
        return HoursPerPayFrequency;
    }

    public void setHoursPerPayFrequency(java.math.BigDecimal value) {
        HoursPerPayFrequency = value;
    }

    /**
    * Category
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

    /**
    * Memo
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * Payroll categories
    */
    private EmployeeStandardPayCategory[] payrollCategories;
    public EmployeeStandardPayCategory[] getPayrollCategories() {
        return payrollCategories;
    }

    public void setPayrollCategories(EmployeeStandardPayCategory[] value) {
        payrollCategories = value;
    }

}


