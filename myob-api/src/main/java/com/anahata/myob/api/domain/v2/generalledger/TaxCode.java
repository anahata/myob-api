//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.SupplierLink;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeType;

/**
* Describes a tax code resource
*/
public class TaxCode  extends BaseEntity 
{
    /**
    * 3 digit code assigned to the tax code.
    */
    private String Code;
    public String getCode() {
        return Code;
    }

    public void setCode(String value) {
        Code = value;
    }

    /**
    * Description given to the tax code.
    */
    private String Description;
    public String getDescription() {
        return Description;
    }

    public void setDescription(String value) {
        Description = value;
    }

    /**
    * The tax types
    */
    private TaxCodeType Type = TaxCodeType.Consolidated;
    public TaxCodeType getType() {
        return Type;
    }

    public void setType(TaxCodeType value) {
        Type = value;
    }

    /**
    * Rate of tax assigned
    */
    private java.math.BigDecimal Rate;
    public java.math.BigDecimal getRate() {
        return Rate;
    }

    public void setRate(java.math.BigDecimal value) {
        Rate = value;
    }

    /**
    * Indicates if the rate will be treated as a negative number i.e. see 
    *  {@link #TaxCodeType.WithholdingsTax}
    *  and 
    *  {@link #TaxCodeType.NoABN_TFN}
    */
    private boolean IsRateNegative;
    public boolean getIsRateNegative() {
        return IsRateNegative;
    }

    public void setIsRateNegative(boolean value) {
        IsRateNegative = value;
    }

    /**
    * A link to the 
    *  {@link #Account}
    *  resource to use for tax collected
    */
    private AccountLink TaxCollectedAccount;
    public AccountLink getTaxCollectedAccount() {
        return TaxCollectedAccount;
    }

    public void setTaxCollectedAccount(AccountLink value) {
        TaxCollectedAccount = value;
    }

    /**
    * A link to the 
    *  {@link #Account}
    *  resource to use for tax paid
    */
    private AccountLink TaxPaidAccount;
    public AccountLink getTaxPaidAccount() {
        return TaxPaidAccount;
    }

    public void setTaxPaidAccount(AccountLink value) {
        TaxPaidAccount = value;
    }

    /**
    * /// 
    * A link to the 
    *  {@link #Account}
    *  resource to use for withholding credit
    */
    private AccountLink WithholdingCreditAccount;
    public AccountLink getWithholdingCreditAccount() {
        return WithholdingCreditAccount;
    }

    public void setWithholdingCreditAccount(AccountLink value) {
        WithholdingCreditAccount = value;
    }

    /**
    * A link to the 
    *  {@link #Account}
    *  resource to use for withholding payable
    */
    private AccountLink WithholdingPayableAccount;
    public AccountLink getWithholdingPayableAccount() {
        return WithholdingPayableAccount;
    }

    public void setWithholdingPayableAccount(AccountLink value) {
        WithholdingPayableAccount = value;
    }

    /**
    * A link to the 
    *  {@link #Account}
    *  resource to use for import duty payable
    */
    private AccountLink ImportDutyPayableAccount;
    public AccountLink getImportDutyPayableAccount() {
        return ImportDutyPayableAccount;
    }

    public void setImportDutyPayableAccount(AccountLink value) {
        ImportDutyPayableAccount = value;
    }

    /**
    * A link to a 
    *  {@link #Supplier}
    *  resource
    */
    private SupplierLink LinkedSupplier;
    public SupplierLink getLinkedSupplier() {
        return LinkedSupplier;
    }

    public void setLinkedSupplier(SupplierLink value) {
        LinkedSupplier = value;
    }

    /**
    * Value which must be exceeded before tax is calculated using this tax code.
    */
    private java.math.BigDecimal LuxuryCarTaxThreshold;
    public java.math.BigDecimal getLuxuryCarTaxThreshold() {
        return LuxuryCarTaxThreshold;
    }

    public void setLuxuryCarTaxThreshold(java.math.BigDecimal value) {
        LuxuryCarTaxThreshold = value;
    }

}


