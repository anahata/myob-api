//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.inventory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.JobLink;
import java.math.BigDecimal;

/**
* An entry adjustment
*/
public class InventoryAdjustmentLine   
{
    /**
    * The row id (required to update an existing entry)
    */
    private int RowID;
    public int getRowID() {
        return RowID;
    }

    public void setRowID(int value) {
        RowID = value;
    }

    /**
    * The adjustment quantity (defaults to 0)
    */
    private java.math.BigDecimal Quantity;
    public java.math.BigDecimal getQuantity() {
        return Quantity;
    }

    public void setQuantity(java.math.BigDecimal value) {
        Quantity = value;
    }

    /**
    * The unit cost of the item, defaults to average cost if not supplied
    */
    private BigDecimal UnitCost;
    public BigDecimal getUnitCost() {
        return UnitCost;
    }

    public void setUnitCost(BigDecimal value) {
        UnitCost = value;
    }

    /**
    * The amount for the line (
    *  {@link #Quantity}
    * /
    *  {@link #UnitCost}
    * )
    */
    private BigDecimal Amount;
    public BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(BigDecimal value) {
        Amount = value;
    }

    /**
    * The item being adjusted (required)
    */
    private ItemLink Item;
    public ItemLink getItem() {
        return Item;
    }

    public void setItem(ItemLink value) {
        Item = value;
    }

    /**
    * The account affected (required if calculated 
    *  {@link #Amount}
    *  > 0)
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * The related Job (optional)
    */
    private JobLink Job;
    public JobLink getJob() {
        return Job;
    }

    public void setJob(JobLink value) {
        Job = value;
    }

    /**
    * A description of the line adjustment
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * Incrementing number that can be used for change control but does does not preserve a date or a time.
    * ONLY required for updating an existing sale line.NOT required when creating a new sale invoice.
    */
    private String RowVersion;
    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String value) {
        RowVersion = value;
    }

}


