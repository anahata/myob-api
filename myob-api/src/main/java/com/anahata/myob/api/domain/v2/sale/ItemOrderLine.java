//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.JobLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import com.anahata.myob.api.domain.v2.inventory.ItemLink;
import com.anahata.myob.api.domain.v2.sale.OrderLine;

/**
* Describes a line for an 
*  {@link #ItemOrder}
*/
public class ItemOrderLine  extends OrderLine 
{
    /**
    * The quanity to be shipped
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private java.math.BigDecimal ShipQuantity;
    public java.math.BigDecimal getShipQuantity() {
        return ShipQuantity;
    }

    public void setShipQuantity(java.math.BigDecimal value) {
        ShipQuantity = value;
    }

    /**
    * The unit price (depends on the value of 
    *  {@link #Order.IsTaxInclusive}
    *  in the parent 
    *  {@link #Order}
    * )
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private java.math.BigDecimal UnitPrice;
    public java.math.BigDecimal getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(java.math.BigDecimal value) {
        UnitPrice = value;
    }

    /**
    * The dicount applied
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private java.math.BigDecimal DiscountPercent;
    public java.math.BigDecimal getDiscountPercent() {
        return DiscountPercent;
    }

    public void setDiscountPercent(java.math.BigDecimal value) {
        DiscountPercent = value;
    }

    /**
    * The line amount (depends on the value of 
    *  {@link #Order.IsTaxInclusive}
    *  in the parent 
    *  {@link #Order}
    * )
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private java.math.BigDecimal Total;
    public java.math.BigDecimal getTotal() {
        return Total;
    }

    public void setTotal(java.math.BigDecimal value) {
        Total = value;
    }

    /**
    * The item
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private ItemLink Item;
    public ItemLink getItem() {
        return Item;
    }

    public void setItem(ItemLink value) {
        Item = value;
    }

    /**
    * The job
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private JobLink Job;
    public JobLink getJob() {
        return Job;
    }

    public void setJob(JobLink value) {
        Job = value;
    }

    /**
    * The applied tax code
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

}


