//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;

/**
* Describes an Account resource
*/
public class Account  extends BaseEntity 
{
    /**
    * Account code format includes separator ie 1-1100
    */
    private String DisplayID;
    public String getDisplayID() {
        return DisplayID;
    }

    public void setDisplayID(String value) {
        DisplayID = value;
    }

    /**
    * Name of the account.
    */
    private String Name;
    public String getName() {
        return Name;
    }

    public void setName(String value) {
        Name = value;
    }

    /**
    * The account classification. (Read only)
    * 
    * You change this field by modifying the Type property
    */
    private Classification classification = Classification.Asset;
    public Classification getClassification() {
        return classification;
    }

    public void setClassification(Classification value) {
        classification = value;
    }

    /**
    * Account number for example 1150. Must be a unique four-digit number that does not include the account type classification number and account separator. (Required)
    * 
    * Required. Initially defaults to -1 which will fail validation on the server if not set.
    */
    private int Number;
    public int getNumber() {
        return Number;
    }

    public void setNumber(int value) {
        Number = value;
    }

    /**
    * Description of the Account
    */
    private String Description;
    public String getDescription() {
        return Description;
    }

    public void setDescription(String value) {
        Description = value;
    }

    /**
    * Identifies the parent account. (Read only)
    */
    private AccountLink ParentAccount;
    public AccountLink getParentAccount() {
        return ParentAccount;
    }

    public void setParentAccount(AccountLink value) {
        ParentAccount = value;
    }

    /**
    * The account active status. 
    * True indicates the account is active. 
    * False indicates the account is inactive.
    * 
    * A user marks an account as inactive when they no er need to record transactions to it. An inactive account doesn't appear in selection lists, but it can still be assigned to a transaction, and be included in reports.
    */
    private boolean IsActive;
    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean value) {
        IsActive = value;
    }

    /**
    * Tax Code associated with the account
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

    /**
    * The hierarchial level of the account in the Accounts List.
    * 
    * Possible values are 1, 2, 3, 4. The highest level accounts are level 1, the lowest 4. You can only assign levels 2 to 4 to a new account.
    */
    private short Level;
    public short getLevel() {
        return Level;
    }

    public void setLevel(short value) {
        Level = value;
    }

    /**
    * Depending on the classification of the account (e.g. asset), you can define the account's type.
    */
    private AccountType Type = AccountType.Bank;
    public AccountType getType() {
        return Type;
    }

    public void setType(AccountType value) {
        Type = value;
    }

    /**
    * Balance of the account as at the conversion date set for the company file.
    */
    private java.math.BigDecimal OpeningBalance;
    public java.math.BigDecimal getOpeningBalance() {
        return OpeningBalance;
    }

    public void setOpeningBalance(java.math.BigDecimal value) {
        OpeningBalance = value;
    }

    /**
    * Current balance of the account. Note that this balance will include all future-dated activity.
    */
    private java.math.BigDecimal CurrentBalance;
    public java.math.BigDecimal getCurrentBalance() {
        return CurrentBalance;
    }

    public void setCurrentBalance(java.math.BigDecimal value) {
        CurrentBalance = value;
    }

    /**
    * The bank details associated with the account.
    */
    private BankingDetails BankingDetails;
    public BankingDetails getBankingDetails() {
        return BankingDetails;
    }

    public void setBankingDetails(BankingDetails value) {
        BankingDetails = value;
    }

    /**
    * Header account status. Header accounts are used to organise, group and subtotal accounts in the Accounts List and reports.
    * 
    * True indicates the account is a header account. Header accounts are used to organise, group and subtotal accounts in the Accounts List and reports. 
    * False indicates the account is a detail account. Only detail accounts can be assigned to transactions.
    */
    private boolean IsHeader;
    public boolean getIsHeader() {
        return IsHeader;
    }

    public void setIsHeader(boolean value) {
        IsHeader = value;
    }

}


