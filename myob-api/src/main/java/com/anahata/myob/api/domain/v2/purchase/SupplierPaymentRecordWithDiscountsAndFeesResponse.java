//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.purchase;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;

/**
* Describe the response of SupplierPayment/RecordWithDiscountsAndFees resource
*/
public class SupplierPaymentRecordWithDiscountsAndFeesResponse   
{
    /**
    * Link to generated SupplierPayment record
    */
    private SupplierPaymentLink SupplierPayment;
    public SupplierPaymentLink getSupplierPayment() {
        return SupplierPayment;
    }

    public void setSupplierPayment(SupplierPaymentLink value) {
        SupplierPayment = value;
    }

    /**
    * Link to generated finance charge invoice due to applied finance charge
    */
    private BillLink FinanceChargeBill;
    public BillLink getFinanceChargeBill() {
        return FinanceChargeBill;
    }

    public void setFinanceChargeBill(BillLink value) {
        FinanceChargeBill = value;
    }

    /**
    * The generated negative purchase bills due to applied discounts
    */
    private ArrayList<BillLink> DiscountAppliedBills = new ArrayList<BillLink>();
    public ArrayList<BillLink> getDiscountAppliedBills() {
        return DiscountAppliedBills;
    }

    public void setDiscountAppliedBills(ArrayList<BillLink> value) {
        DiscountAppliedBills = value;
    }

    /**
    * The generated credit settlements due to applied discounts
    */
    private ArrayList<DebitSettlementLink> DebitSettlements = new ArrayList<DebitSettlementLink>();
    public ArrayList<DebitSettlementLink> getDebitSettlements() {
        return DebitSettlements;
    }

    public void setDebitSettlements(ArrayList<DebitSettlementLink> value) {
        DebitSettlements = value;
    }

}


