//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.sale.CalculateDiscountsFeesSale;

/**
* Describe the Sale/CustomerPayment/CalculateDiscountsFees response item
*/
public class CalculateDiscountsFeesResponseItem   
{
    /**
    * Unique id to allow you to identify the discount calculated item
    */
    private int RequestID;
    public int getRequestID() {
        return RequestID;
    }

    public void setRequestID(int value) {
        RequestID = value;
    }

    /**
    * Sale use for calculation
    */
    private CalculateDiscountsFeesSale Sale;
    public CalculateDiscountsFeesSale getSale() {
        return Sale;
    }

    public void setSale(CalculateDiscountsFeesSale value) {
        Sale = value;
    }

    /**
    * Payment date
    */
    private java.util.Date PaymentDate = new java.util.Date();
    public java.util.Date getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(java.util.Date value) {
        PaymentDate = value;
    }

    /**
    * Calculated discount
    */
    private java.math.BigDecimal Discount;
    public java.math.BigDecimal getDiscount() {
        return Discount;
    }

    public void setDiscount(java.math.BigDecimal value) {
        Discount = value;
    }

    /**
    * Discount expiry date
    */
    private java.util.Date DiscountExpiryDate = new java.util.Date();
    public java.util.Date getDiscountExpiryDate() {
        return DiscountExpiryDate;
    }

    public void setDiscountExpiryDate(java.util.Date value) {
        DiscountExpiryDate = value;
    }

    /**
    * Calculated finance charges
    */
    private java.math.BigDecimal FinanceCharge;
    public java.math.BigDecimal getFinanceCharge() {
        return FinanceCharge;
    }

    public void setFinanceCharge(java.math.BigDecimal value) {
        FinanceCharge = value;
    }

    /**
    * Balance due date
    */
    private java.util.Date BalanceDueDate = new java.util.Date();
    public java.util.Date getBalanceDueDate() {
        return BalanceDueDate;
    }

    public void setBalanceDueDate(java.util.Date value) {
        BalanceDueDate = value;
    }

    /**
    * Balance due amount
    */
    private java.math.BigDecimal BalanceDue;
    public java.math.BigDecimal getBalanceDue() {
        return BalanceDue;
    }

    public void setBalanceDue(java.math.BigDecimal value) {
        BalanceDue = value;
    }

}


