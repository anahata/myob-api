//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.util.lang.AnahataStringUtils;
import java.io.Serializable;

/**
* Describes an address for a Contact
*/
public class Address implements Serializable
        
{
    /**
    * The address slot location 1..5
    */
    private int Location;
    public int getLocation() {
        return Location;
    }

    public void setLocation(int value) {
        Location = value;
    }

    /**
    * The street
    */
    private String Street;
    public String getStreet() {
        return Street;
    }

    public void setStreet(String value) {
        Street = value;
    }

    /**
    * The city
    */
    private String City;
    public String getCity() {
        return City;
    }

    public void setCity(String value) {
        City = value;
    }

    /**
    * The state
    */
    private String State;
    public String getState() {
        return State;
    }

    public void setState(String value) {
        State = value;
    }

    /**
    * The postcode
    */
    private String PostCode;
    public String getPostCode() {
        return PostCode;
    }

    public void setPostCode(String value) {
        PostCode = value;
    }

    /**
    * The country
    */
    private String Country;
    public String getCountry() {
        return Country;
    }

    public void setCountry(String value) {
        Country = value;
    }

    /**
    * A phone number
    */
    private String Phone1;
    public String getPhone1() {
        return Phone1;
    }

    public void setPhone1(String value) {
        Phone1 = value;
    }

    /**
    * A phone number
    */
    private String Phone2;
    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String value) {
        Phone2 = value;
    }

    /**
    * A phone number
    */
    private String Phone3;
    public String getPhone3() {
        return Phone3;
    }

    public void setPhone3(String value) {
        Phone3 = value;
    }

    /**
    * A fax number
    */
    private String Fax;
    public String getFax() {
        return Fax;
    }

    public void setFax(String value) {
        Fax = value;
    }

    /**
    * An email address
    */
    private String Email;
    public String getEmail() {
        return Email;
    }

    public void setEmail(String value) {
        Email = value;
    }

    /**
    * A website
    */
    private String Website;
    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String value) {
        Website = value;
    }

    /**
    * The primary contact name
    */
    private String ContactName;
    public String getContactName() {
        return ContactName;
    }

    public void setContactName(String value) {
        ContactName = value;
    }

    /**
    * Mr, Mrs, Miss, ...
    */
    private String Salutation;
    public String getSalutation() {
        return Salutation;
    }

    public void setSalutation(String value) {
        Salutation = value;
    }
    
    public boolean isBlank() {
        return AnahataStringUtils.isAllEmpty(City, ContactName, Country, Email, Fax, Phone1, Phone2, Phone3, PostCode, Salutation, State, Street, Website);
    }

}


