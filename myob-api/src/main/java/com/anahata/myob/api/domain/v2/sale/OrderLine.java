//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.sale.OrderLineType;

/**
* Describes a basic orderline
*/
public abstract class OrderLine   
{
    /**
    * Sequence of the entry within the item sale set.
    * ONLY required for updating an existing order line.NOT required when creating a new order.
    */
    private int RowID;
    public int getRowID() {
        return RowID;
    }

    public void setRowID(int value) {
        RowID = value;
    }

    /**
    * The type of order line
    */
    private OrderLineType Type = OrderLineType.Transaction;
    public OrderLineType getType() {
        return Type;
    }

    public void setType(OrderLineType value) {
        Type = value;
    }

    /**
    * The line description
    */
    private String Description;
    public String getDescription() {
        return Description;
    }

    public void setDescription(String value) {
        Description = value;
    }

    /**
    * Incrementing number that can be used for change control but does does not preserve a date or a time.
    * ONLY required for updating an existing line.NOT required when creating a new order.
    */
    private String RowVersion;
    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String value) {
        RowVersion = value;
    }

}


