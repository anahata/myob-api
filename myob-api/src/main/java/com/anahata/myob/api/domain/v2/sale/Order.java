//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.CustomerLink;
import com.anahata.myob.api.domain.v2.contact.EmployeeLink;
import com.anahata.myob.api.domain.v2.generalledger.CategoryLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import java.math.BigDecimal;

/**
* Describes a basic sale order
*/
public class Order  extends BaseEntity 
{
    /**
    * Initialise
    */
    public Order()  {
        
    }

    /**
    * The number
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * Oder date entry
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * Customer PO number
    */
    private String CustomerPurchaseOrderNumber;
    public String getCustomerPurchaseOrderNumber() {
        return CustomerPurchaseOrderNumber;
    }

    public void setCustomerPurchaseOrderNumber(String value) {
        CustomerPurchaseOrderNumber = value;
    }

    /**
    * The customer
    */
    private CustomerLink Customer;
    public CustomerLink getCustomer() {
        return Customer;
    }

    public void setCustomer(CustomerLink value) {
        Customer = value;
    }

    /**
    * Customer payment terms
    */
    private OrderTerms Terms;
    public OrderTerms getTerms() {
        return Terms;
    }

    public void setTerms(OrderTerms value) {
        Terms = value;
    }

    /**
    * True indicates the transaction is set to tax inclusive.
    * False indicates the transaction is not tax inclusive.
    */
    private boolean IsTaxInclusive;
    public boolean getIsTaxInclusive() {
        return IsTaxInclusive;
    }

    public void setIsTaxInclusive(boolean value) {
        IsTaxInclusive = value;
    }

    /**
    * Sum of all tax exclusive line amounts applicable to the sale invoice.
    */
    private java.math.BigDecimal Subtotal;
    public java.math.BigDecimal getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(java.math.BigDecimal value) {
        Subtotal = value;
    }

    /**
    * Tax freight amount applicable to the sale order.
    * 
    * Only supported by Item or Service orders
    */
    private BigDecimal Freight;
    public BigDecimal getFreight() {
        return Freight;
    }

    public void setFreight(BigDecimal value) {
        Freight = value;
    }

    /**
    * The freight Tax code
    * 
    * Only supported by Item or Service orders
    */
    private TaxCodeLink FreightTaxCode;
    public TaxCodeLink getFreightTaxCode() {
        return FreightTaxCode;
    }

    public void setFreightTaxCode(TaxCodeLink value) {
        FreightTaxCode = value;
    }

    /**
    * Total of all tax amounts applicable to the sale invoice.
    */
    private java.math.BigDecimal TotalTax;
    public java.math.BigDecimal getTotalTax() {
        return TotalTax;
    }

    public void setTotalTax(java.math.BigDecimal value) {
        TotalTax = value;
    }

    /**
    * Total amount of the sale invoice.
    */
    private java.math.BigDecimal TotalAmount;
    public java.math.BigDecimal getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(java.math.BigDecimal value) {
        TotalAmount = value;
    }

    /**
    * The category assocated with the Invoice
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

    /**
    * The employee contact
    */
    private EmployeeLink Salesperson;
    public EmployeeLink getSalesperson() {
        return Salesperson;
    }

    public void setSalesperson(EmployeeLink value) {
        Salesperson = value;
    }

    /**
    * Journal memo text describing the sale.
    */
    private String JournalMemo;
    public String getJournalMemo() {
        return JournalMemo;
    }

    public void setJournalMemo(String value) {
        JournalMemo = value;
    }

    /**
    * Referral Source selected on the sale invoice.
    */
    private String ReferralSource;
    public String getReferralSource() {
        return ReferralSource;
    }

    public void setReferralSource(String value) {
        ReferralSource = value;
    }

    /**
    * The amount paid to date
    */
    private java.math.BigDecimal AppliedToDate;
    public java.math.BigDecimal getAppliedToDate() {
        return AppliedToDate;
    }

    public void setAppliedToDate(java.math.BigDecimal value) {
        AppliedToDate = value;
    }

    /**
    * The balance due
    */
    private java.math.BigDecimal BalanceDueAmount;
    public java.math.BigDecimal getBalanceDueAmount() {
        return BalanceDueAmount;
    }

    public void setBalanceDueAmount(java.math.BigDecimal value) {
        BalanceDueAmount = value;
    }

    /**
    * The current status of the order
    */
    private OrderStatus Status = OrderStatus.Open;
    public OrderStatus getStatus() {
        return Status;
    }

    public void setStatus(OrderStatus value) {
        Status = value;
    }

    /**
    * The date of the last payment made on the order
    */
    private java.util.Date LastPaymentDate = new java.util.Date();
    public java.util.Date getLastPaymentDate() {
        return LastPaymentDate;
    }

    public void setLastPaymentDate(java.util.Date value) {
        LastPaymentDate = value;
    }

    /**
    * The type of order
    */
    private OrderLayoutType OrderType = OrderLayoutType.Service;
    public OrderLayoutType getOrderType() {
        return OrderType;
    }

    public void setOrderType(OrderLayoutType value) {
        OrderType = value;
    }

}


