//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.sale.CreditSettlementLineType;
import com.anahata.myob.api.domain.v2.sale.SaleLink;

/**
* Describe the Sale/CreditSettlement's line
*/
public class CreditSettlementLine   
{
    /**
    * Indicates whether the settlement was to an Invoice or Order
    */
    private CreditSettlementLineType Type = CreditSettlementLineType.Invoice;
    public CreditSettlementLineType getType() {
        return Type;
    }

    public void setType(CreditSettlementLineType value) {
        Type = value;
    }

    /**
    * A link to the applied Invoice or Order
    */
    private SaleLink Sale;
    public SaleLink getSale() {
        return Sale;
    }

    public void setSale(SaleLink value) {
        Sale = value;
    }

    /**
    * The amount applied to the Invoice or Order
    */
    private java.math.BigDecimal AmountApplied;
    public java.math.BigDecimal getAmountApplied() {
        return AmountApplied;
    }

    public void setAmountApplied(java.math.BigDecimal value) {
        AmountApplied = value;
    }

}


