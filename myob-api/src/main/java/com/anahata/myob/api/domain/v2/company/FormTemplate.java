//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.company;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
* Printed Form Template
*/
public class FormTemplate   
{
    /**
    * Form name
    */
    private String Name;
    public String getName() {
        return Name;
    }

    public void setName(String value) {
        Name = value;
    }

    /**
    * Type
    */
    private FormTemplateType Type = FormTemplateType.Sale;
    public FormTemplateType getType() {
        return Type;
    }

    public void setType(FormTemplateType value) {
        Type = value;
    }

    /**
    * Form Layout where applicable (e.g. Invoice, Order, Bill)
    */
    private FormTemplateTypeLayout Layout = FormTemplateTypeLayout.Item;
    public FormTemplateTypeLayout getLayout() {
        return Layout;
    }

    public void setLayout(FormTemplateTypeLayout value) {
        Layout = value;
    }

    /**
    * Template is System (e.g. not Custom)
    */
    private boolean IsSystem;
    public boolean getIsSystem() {
        return IsSystem;
    }

    public void setIsSystem(boolean value) {
        IsSystem = value;
    }

}


