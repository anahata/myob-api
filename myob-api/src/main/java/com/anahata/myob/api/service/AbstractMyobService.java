/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.service;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.myob.api.MyobEndPoint;
import com.anahata.myob.api.MyobEndPointProvider;
import com.anahata.myob.api.auth.OAuthAccessToken;
import com.anahata.myob.api.service.exception.MyobException;
import com.anahata.myob.api.transport.AbstractMyobTransport;
import com.anahata.myob.api.transport.HttpClientMyobTransport;
import com.anahata.util.cdi.Cdi;
import com.google.gson.GsonBuilder;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author pablo
 */
@Slf4j
public abstract class AbstractMyobService {
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    private static final GsonBuilder GSON_BUILDER = new GsonBuilder().setDateFormat(DATE_FORMAT).setPrettyPrinting();

    @Getter(AccessLevel.PROTECTED)
    private AbstractMyobTransport transport = new HttpClientMyobTransport();

    @Setter
    @Getter
    private MyobEndPointProvider endPointProvider;

    protected final String context;

    protected AbstractMyobService(String context) {
        this.context = context;
    }

    protected static <X> X fromJson(String json, Class<X> type) {
        X item = GSON_BUILDER.create().fromJson(json, type);
        return item;
    }

    protected static String toJson(Object o) {
        String s = GSON_BUILDER.create().toJson(o);
        return s;
    }

    /**
     * Gets the endpoint from the provider and refreshes the token if required.
     *
     * @return
     */
    protected MyobEndPoint getEndpoint() {
        MyobEndPointProvider mepProvider = endPointProvider;

        if (mepProvider == null) {
            try {
                mepProvider = Cdi.get(MyobEndPointProvider.class);
            } catch (Exception e) {
                log.debug("EndPointProvider not ser and could not be looked up via CDI", e);
                throw new MyobException("EndPointProvider not ser and could not be looked up via CDI", e);
            }
        }

        try {
            return mepProvider.refreshTokenIfRequiredAndGetEndPoint();
        } catch (Exception e) {
            log.error("Exception on MYOB Service", e);
            throw e instanceof MyobException ? (MyobException)e : new MyobException(e);
        }
    }

    //protected String sendReceiveURL(String method, String params, String uId) throws MyobException {
    /**
     * Performs a a send-receive given a full URL.
     *
     * @param url
     * @param method
     * @param payLoad
     * @return
     * @throws MyobException
     */
    protected String sendReceiveURL(URI url, String method, String payLoad) throws MyobException {
        try {
            return transport.sendReceiveURL(getEndpoint(), url, method, payLoad);
        } catch (Exception e) {
            log.error("Exception on MYOB Service", e);
            throw e instanceof MyobException ? (MyobException)e : new MyobException(e);
        }
    }

    /**
     * Performs a send-receive building the final URL.
     *
     * @param uId
     * @param queryString
     * @param method
     * @param payLoad
     * @return
     * @throws MyobException
     */
    protected String sendReceive(String uId, String queryString, String method, String payLoad) throws MyobException {
        try {
            MyobEndPoint ep = getEndpoint();
            OAuthAccessToken oaat = ep.getOauthAccessToken();
            return transport.sendReceive(ep, context, uId, queryString, method, payLoad);
        } catch (Exception e) {
            log.error("Exception on MYOB Service", e);
            throw e instanceof MyobException ? (MyobException)e : new MyobException(e);
        }
    }

    public static void main(String[] args) throws Exception {
        Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse("2014-12-06T17:27:07");
        System.out.println(date);
    }

}
