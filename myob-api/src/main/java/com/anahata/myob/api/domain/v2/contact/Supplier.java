//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.contact.Contact;
import com.anahata.myob.api.domain.v2.contact.SupplierBuyingDetails;
import com.anahata.myob.api.domain.v2.contact.SupplierPaymentDetails;

/**
* Describes the Supplier resource
*/
public class Supplier  extends Contact 
{
    /**
    * The default buying details
    */
    private SupplierBuyingDetails BuyingDetails;
    public SupplierBuyingDetails getBuyingDetails() {
        return BuyingDetails;
    }

    public void setBuyingDetails(SupplierBuyingDetails value) {
        BuyingDetails = value;
    }

    /**
    * The default payment details
    */
    private SupplierPaymentDetails PaymentDetails;
    public SupplierPaymentDetails getPaymentDetails() {
        return PaymentDetails;
    }

    public void setPaymentDetails(SupplierPaymentDetails value) {
        PaymentDetails = value;
    }

}


