//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.payrollCategory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.math.BigDecimal;

/**
* Describes how the 
*  {@link #PayrollCategorySuperannuation}
*  is calculated
*/
public class PayrollCategorySuperannuationCalculationBasis   
{
    /**
    * The type of calculation
    */
    private PayrollCategorySuperannuationCalculationBasisType Type = PayrollCategorySuperannuationCalculationBasisType.UserEntered;
    public PayrollCategorySuperannuationCalculationBasisType getType() {
        return Type;
    }

    public void setType(PayrollCategorySuperannuationCalculationBasisType value) {
        Type = value;
    }

    /**
    * The calculation is the percentage of the 
    *  {@link #MYOB.AccountRight.SDK.Contracts.Version2.PayrollCategory.PayrollCategorySuperannuationCalculationBasis.PayrollCategory}
    */
    private BigDecimal PercentageOf;
    public BigDecimal getPercentageOf() {
        return PercentageOf;
    }

    public void setPercentageOf(BigDecimal value) {
        PercentageOf = value;
    }

    /**
    * The 
    *  {@link #PayrollCategory}
    *  whose percentge of makes up the calculation
    */
    private PayrollCategoryLink PayrollCategory;
    public PayrollCategoryLink getPayrollCategory() {
        return PayrollCategory;
    }

    public void setPayrollCategory(PayrollCategoryLink value) {
        PayrollCategory = value;
    }

    /**
    * The calculation is based on a fixed amount of dollars per 
    *  {@link #MYOB.AccountRight.SDK.Contracts.Version2.PayrollCategory.PayrollCategorySuperannuationCalculationBasis.AccrualPeriod}
    */
    private BigDecimal FixedDollarsOf;
    public BigDecimal getFixedDollarsOf() {
        return FixedDollarsOf;
    }

    public void setFixedDollarsOf(BigDecimal value) {
        FixedDollarsOf = value;
    }

    /**
    * The period over which the total dollars amount is calculated
    */
    private PayrollCategorySuperannuationAccrualPeriod AccrualPeriod = PayrollCategorySuperannuationAccrualPeriod.PayPeriod;
    public PayrollCategorySuperannuationAccrualPeriod getAccrualPeriod() {
        return AccrualPeriod;
    }

    public void setAccrualPeriod(PayrollCategorySuperannuationAccrualPeriod value) {
        AccrualPeriod = value;
    }

}


