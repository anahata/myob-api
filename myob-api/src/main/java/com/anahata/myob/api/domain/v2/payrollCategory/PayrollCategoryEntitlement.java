//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.payrollCategory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;

/**
* Describes the Entitlement PayrollCategory
*/
public class PayrollCategoryEntitlement  extends PayrollCategory 
{
    /**
    * Print details on the pay advice
    */
    private boolean PrintOnPayAdvice;
    public boolean getPrintOnPayAdvice() {
        return PrintOnPayAdvice;
    }

    public void setPrintOnPayAdvice(boolean value) {
        PrintOnPayAdvice = value;
    }

    /**
    * Entitlements earned can carry into the next fianacial year
    */
    private boolean CarryEntitlementOverToNextYear;
    public boolean getCarryEntitlementOverToNextYear() {
        return CarryEntitlementOverToNextYear;
    }

    public void setCarryEntitlementOverToNextYear(boolean value) {
        CarryEntitlementOverToNextYear = value;
    }

    /**
    * Details on how the entitlements are calculated
    */
    private PayrollCategoryEntitlementCalculationBasis CalculationBasis;
    public PayrollCategoryEntitlementCalculationBasis getCalculationBasis() {
        return CalculationBasis;
    }

    public void setCalculationBasis(PayrollCategoryEntitlementCalculationBasis value) {
        CalculationBasis = value;
    }

    /**
    * Related 
    *  {@link #PayrollCategoryWage}
    */
    private ArrayList<PayrollCategoryLink> LinkedPayrollCategoryWages = new ArrayList<PayrollCategoryLink>();
    public ArrayList<PayrollCategoryLink> getLinkedPayrollCategoryWages() {
        return LinkedPayrollCategoryWages;
    }

    public void setLinkedPayrollCategoryWages(ArrayList<PayrollCategoryLink> value) {
        LinkedPayrollCategoryWages = value;
    }

    /**
    * Exempt this 
    *  {@link #PayrollCategory}
    *  from deduction and tax calculations
    */
    private ArrayList<PayrollCategoryLink> Exemptions = new ArrayList<PayrollCategoryLink>();
    public ArrayList<PayrollCategoryLink> getExemptions() {
        return Exemptions;
    }

    public void setExemptions(ArrayList<PayrollCategoryLink> value) {
        Exemptions = value;
    }

}


