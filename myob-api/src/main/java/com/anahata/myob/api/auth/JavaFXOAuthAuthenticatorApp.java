/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.auth;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.myob.api.MyobEndPoint;
import com.anahata.myob.api.MyobEndPointProvider;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Document;
import static com.anahata.myob.api.auth.OauthAuthenticator.*;
import static java.net.URLDecoder.*;

/**
 *
 * @author pablo
 */
@Slf4j
public class JavaFXOAuthAuthenticatorApp extends Application {
    private static final String CODE_PARAMETER = "code=";

    private static MyobPlugin plugin;

    private static OauthLoginCallBack callBack;

    public static OAuthAccessToken ret;

    private static Stage mainStage;

    public static void loginAndGetToken(MyobPlugin plugin) throws Exception {
        JavaFXOAuthAuthenticatorApp.plugin = plugin;
        JavaFXOAuthAuthenticatorApp.callBack = new OauthLoginCallBack() {
            @Override
            public void userLogon(String code) {
                try {
                    ret = OauthAuthenticator.getAccessToken(plugin, code);
                } catch (Exception e) {
                    log.error("Exception getting access token", e);
                    throw new RuntimeException(e);
                }
                Platform.exit();
                //mainStage.close();
            }
        };
        launch();

    }

    @Override
    public void start(Stage stage) throws Exception {
        mainStage = stage;
        login(stage, plugin, callBack);
    }

    /**
     * Login using JavaFX WebView.
     *
     * @param stage    the stage
     * @param callBack the callBack
     * @throws Exception
     */
    public static void login(@NonNull Stage stage, @NonNull MyobPlugin plugin, @NonNull OauthLoginCallBack callBack)
            throws Exception {

        WebView wb = new WebView();
        
        stage.setWidth(500);
        stage.setHeight(500);
        stage.setScene(new Scene(wb));
        stage.show();

        //uncomment this code if cookies driving you crazy:
//        
//        java.net.CookieManager manager = new java.net.CookieManager(null, CookiePolicy.ACCEPT_ALL);
//        CookieStore store = manager.getCookieStore();
//        log.debug("Cookie Store = {}", store);
//        CookieManager.setDefault(new CookieHandler() {
//            @Override
//            public Map<String, List<String>> get(URI uri, Map<String, List<String>> requestHeaders) throws IOException {
//                
//                for (Map.Entry<String, List<String>> e: requestHeaders.entrySet()) {
//                    log.debug("CookieManager get() uri={} {}={}", uri, e.getKey(), e.getValue());
//                }
//                Map<String, List<String>> ret = manager.get(uri, requestHeaders);
//                
//                for (Map.Entry<String, List<String>> e: ret.entrySet()) {
//                    log.debug("CookieManager returning() uri={} {}={}", uri, e.getKey(), e.getValue());
//                }
//                return ret;
//            }
//
//            @Override
//            public void put(URI uri, Map<String, List<String>> responseHeaders) throws IOException {
//                for (Map.Entry<String, List<String>> e: responseHeaders.entrySet()) {
//                    log.debug("CookieManager put() uri={} {}={}", uri, e.getKey(), e.getValue());
//                }
//                manager.put(uri, responseHeaders);
//            }
//        });
        //System.out.println("Default cookie handler is " + cm);
        String url = getloginUrl(plugin.getKey(), plugin.getRedirectURL());

        Runnable checkLocation = new Runnable() {
            @Override
            public void run() {
                String loc = wb.getEngine().getLocation();
                if (loc.contains(CODE_PARAMETER)) {
                    final String code = decode(loc.substring(loc.indexOf(CODE_PARAMETER) + CODE_PARAMETER.length()));
                    log.debug("myob auth code() = " + code);
                    callBack.userLogon(code);
                    stage.close();
                }
            }
        };

        wb.getEngine().documentProperty().addListener(new ChangeListener<Document>() {
            @Override
            public void changed(
                    ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {

                log.debug(
                        "wb.getEngine().getDocument().getLocation() newValue= " + wb.getEngine().getLocation() + " UA=" + wb.getEngine().getUserAgent());
                checkLocation.run();

            }
        });
        wb.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
            @Override
            public void changed(
                    ObservableValue<? extends State> observable, State oldValue, State newValue) {
                log.debug("wb.getEngine().getLoadWorker() URL {} = state = {}", wb.getEngine().getLocation(), newValue);
                checkLocation.run();
            }
        });
        log.debug("Opening {}", url);
        wb.getEngine().load(url);

    }

    public static void loginAndGetToken(MyobEndPointProvider provider) throws Exception {
        Stage stage = new Stage();
        final MyobEndPoint mep = provider.getEndPoint();
        login(stage, mep.getOauthAccessToken().getPlugin(), (String code) -> {
            try {
                OAuthAccessToken token = OauthAuthenticator.getAccessToken(mep.getOauthAccessToken().getPlugin(), code);
                log.debug("Got token {}", token);
                mep.setOauthAccessToken(token);
                provider.oauthTokenRefreshed(mep);
            } catch (Exception e) {
                log.error("Exception getting access token", e);
            }
        });

    }
}
