//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.contact.CardLink;
import com.anahata.myob.api.domain.v2.generalledger.CategoryLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import java.math.BigDecimal;

/**
* Describe the Sale/Invoice resource
*/
public class Invoice  extends Sale 
{
    /**
    * Initialise
    */
    public Invoice()  {
        
    }

    /**
    * The invoice type - this is only populated when querying the "/Sale/Invoice" endpoint
    */
    private InvoiceLayoutType InvoiceType = InvoiceLayoutType.NoDefault;
    public InvoiceLayoutType getInvoiceType() {
        return InvoiceType;
    }

    public void setInvoiceType(InvoiceLayoutType value) {
        InvoiceType = value;
    }

    /**
    * ShipTo address of the sale invoice.
    * 
    * Not supported by Professional or Miscellaneous invoices
    */
    private String ShipToAddress;
    public String getShipToAddress() {
        return ShipToAddress;
    }

    public void setShipToAddress(String value) {
        ShipToAddress = value;
    }

    /**
    * Customer payment terms
    */
    private InvoiceTerms Terms;
    public InvoiceTerms getTerms() {
        return Terms;
    }

    public void setTerms(InvoiceTerms value) {
        Terms = value;
    }

    /**
    * True indicates the transaction is set to tax inclusive.
    * False indicates the transaction is not tax inclusive.
    */
    private boolean IsTaxInclusive;
    public boolean getIsTaxInclusive() {
        return IsTaxInclusive;
    }

    public void setIsTaxInclusive(boolean value) {
        IsTaxInclusive = value;
    }

    /**
    * Sum of all tax exclusive line amounts applicable to the sale invoice.
    */
    private java.math.BigDecimal Subtotal;
    public java.math.BigDecimal getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(java.math.BigDecimal value) {
        Subtotal = value;
    }

    /**
    * Tax freight amount applicable to the sale invoice.
    * 
    * Not supported by Professional or Miscellaneous invoices
    */
    private java.math.BigDecimal Freight;
    public java.math.BigDecimal getFreight() {
        return Freight;
    }

    public void setFreight(java.math.BigDecimal value) {
        Freight = value;
    }

    /**
    * The freight Tax code
    * 
    * Not supported by Professional or Miscellaneous invoices
    */
    private TaxCodeLink FreightTaxCode;
    public TaxCodeLink getFreightTaxCode() {
        return FreightTaxCode;
    }

    public void setFreightTaxCode(TaxCodeLink value) {
        FreightTaxCode = value;
    }

    /**
    * Total of all tax amounts applicable to the sale invoice.
    */
    private BigDecimal TotalTax;
    public BigDecimal getTotalTax() {
        return TotalTax;
    }

    public void setTotalTax(BigDecimal value) {
        TotalTax = value;
    }

    /**
    * Total amount of the sale invoice.
    */
    private BigDecimal TotalAmount;
    public BigDecimal getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(BigDecimal value) {
        TotalAmount = value;
    }

    /**
    * The category associated with the Invoice
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

    /**
    * The employee contact
    */
    private CardLink Salesperson;
    public CardLink getSalesperson() {
        return Salesperson;
    }

    public void setSalesperson(CardLink value) {
        Salesperson = value;
    }

    /**
    * Sale invoice comment.
    * 
    * Not supported by Miscellaneous invoices
    */
    private String Comment;
    public String getComment() {
        return Comment;
    }

    public void setComment(String value) {
        Comment = value;
    }

    /**
    * Shipping method text.
    * 
    * Not supported by Professional or Miscellaneous invoices
    */
    private String ShippingMethod;
    public String getShippingMethod() {
        return ShippingMethod;
    }

    public void setShippingMethod(String value) {
        ShippingMethod = value;
    }

    /**
    * Journal memo text describing the sale.
    */
    private String JournalMemo;
    public String getJournalMemo() {
        return JournalMemo;
    }

    public void setJournalMemo(String value) {
        JournalMemo = value;
    }

    /**
    * Referral Source selected on the sale invoice.
    */
    private String ReferralSource;
    public String getReferralSource() {
        return ReferralSource;
    }

    public void setReferralSource(String value) {
        ReferralSource = value;
    }

    /**
    * The date of the last payment made on the invoice
    * 
    * Availability: 2013.5 (Cloud), 2014.1 (Desktop)
    */
    private java.util.Date LastPaymentDate = new java.util.Date();
    public java.util.Date getLastPaymentDate() {
        return LastPaymentDate;
    }

    public void setLastPaymentDate(java.util.Date value) {
        LastPaymentDate = value;
    }

    /**
    * Invoice delivery status assigned.
    * 
    * Not supported by Miscellaneous invoices
    */
    private DocumentAction InvoiceDeliveryStatus = DocumentAction.Nothing;
    public DocumentAction getInvoiceDeliveryStatus() {
        return InvoiceDeliveryStatus;
    }

    public void setInvoiceDeliveryStatus(DocumentAction value) {
        InvoiceDeliveryStatus = value;
    }

    /**
    * The source Order when an Invoice is converted from an Order
    * or when you wish to convert an existing Open Order to a new Invoice
    * Available from 2014.3
    */
    private SaleOrderLink Order;
    public SaleOrderLink getOrder() {
        return Order;
    }

    public void setOrder(SaleOrderLink value) {
        Order = value;
    }

}


