//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;

/**
* The full CompanyFile resource with available resources
*/
public class CompanyFileWithResources   
{
    /**
    * The CompanyFile details
    */
    private CompanyFile CompanyFile;
    public CompanyFile getCompanyFile() {
        return CompanyFile;
    }

    public void setCompanyFile(CompanyFile value) {
        CompanyFile = value;
    }

    /**
    * The resources available to this company file
    */
    private ArrayList<String> Resources = new ArrayList<String>();
    public ArrayList<String> getResources() {
        return Resources;
    }

    public void setResources(ArrayList<String> value) {
        Resources = value;
    }

}


