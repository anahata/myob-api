//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.purchase;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.inventory.ItemLink;
import com.anahata.myob.api.domain.v2.purchase.BillLine;

/**
* Describe the Purchase/Bill/Item's Lines
*/
public class ItemBillLine  extends BillLine 
{
    /**
    * The quantity of goods shipped.
    */
    private java.math.BigDecimal BillQuantity;
    public java.math.BigDecimal getBillQuantity() {
        return BillQuantity;
    }

    public void setBillQuantity(java.math.BigDecimal value) {
        BillQuantity = value;
    }

    /**
    * The quantity of goods received.
    */
    private java.math.BigDecimal ReceivedQuantity;
    public java.math.BigDecimal getReceivedQuantity() {
        return ReceivedQuantity;
    }

    public void setReceivedQuantity(java.math.BigDecimal value) {
        ReceivedQuantity = value;
    }

    /**
    * The quantity of goods to back order
    * To be implemented when Purchase Order functionality is available.
    */
    private java.math.BigDecimal BackorderQuantity;
    public java.math.BigDecimal getBackorderQuantity() {
        return BackorderQuantity;
    }

    public void setBackorderQuantity(java.math.BigDecimal value) {
        BackorderQuantity = value;
    }

    /**
    * Unit price assigned to the item.
    */
    private java.math.BigDecimal UnitPrice;
    public java.math.BigDecimal getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(java.math.BigDecimal value) {
        UnitPrice = value;
    }

    /**
    * Discount rate applicable to the line of the purchase bill.
    */
    private java.math.BigDecimal DiscountPercent;
    public java.math.BigDecimal getDiscountPercent() {
        return DiscountPercent;
    }

    public void setDiscountPercent(java.math.BigDecimal value) {
        DiscountPercent = value;
    }

    /**
    * Item of the purchase item bill line
    */
    private ItemLink Item;
    public ItemLink getItem() {
        return Item;
    }

    public void setItem(ItemLink value) {
        Item = value;
    }

}


