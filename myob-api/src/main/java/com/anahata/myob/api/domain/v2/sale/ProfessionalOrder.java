//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.sale.DocumentAction;
import com.anahata.myob.api.domain.v2.sale.OrderWithLines;
import com.anahata.myob.api.domain.v2.sale.ProfessionalOrderLine;

/**
* Describes a service order
*/
public class ProfessionalOrder  extends OrderWithLines<ProfessionalOrderLine> 
{
    /**
    * Initialise
    */
    public ProfessionalOrder() throws Exception {
        setDeliveryStatus(DocumentAction.Print);
    }

    /**
    * Sale order comment.
    */
    private String Comment;
    public String getComment() {
        return Comment;
    }

    public void setComment(String value) {
        Comment = value;
    }

    /**
    * The promised date
    */
    private java.util.Date PromisedDate = new java.util.Date();
    public java.util.Date getPromisedDate() {
        return PromisedDate;
    }

    public void setPromisedDate(java.util.Date value) {
        PromisedDate = value;
    }

    /**
    * Invoice delivery status assigned.
    */
    private DocumentAction DeliveryStatus = DocumentAction.Nothing;
    public DocumentAction getDeliveryStatus() {
        return DeliveryStatus;
    }

    public void setDeliveryStatus(DocumentAction value) {
        DeliveryStatus = value;
    }

}


