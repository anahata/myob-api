//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.report.payroll;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.company.CompanyLink;
import com.anahata.myob.api.domain.v2.contact.EmployeeLink;
import com.anahata.myob.api.domain.v2.contact.PayFrequency;
import com.anahata.myob.api.domain.v2.contact.PayrollEmploymentClassificationLink;
import com.anahata.myob.api.domain.v2.contact.SuperannuationFundLink;
import java.util.ArrayList;

/**
* Describes the EmployeePayrollAdvice resource
*/
public class EmployeePayrollAdvice   
{
    /**
    * The Employer
    */
    private CompanyLink Employer;
    public CompanyLink getEmployer() {
        return Employer;
    }

    public void setEmployer(CompanyLink value) {
        Employer = value;
    }

    /**
    * The Employee
    */
    private EmployeeLink Employee;
    public EmployeeLink getEmployee() {
        return Employee;
    }

    public void setEmployee(EmployeeLink value) {
        Employee = value;
    }

    /**
    * The Payment Date
    */
    private java.util.Date PaymentDate = new java.util.Date();
    public java.util.Date getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(java.util.Date value) {
        PaymentDate = value;
    }

    /**
    * Pay Period Start
    */
    private java.util.Date PayPeriodStartDate = new java.util.Date();
    public java.util.Date getPayPeriodStartDate() {
        return PayPeriodStartDate;
    }

    public void setPayPeriodStartDate(java.util.Date value) {
        PayPeriodStartDate = value;
    }

    /**
    * The Pay Period End
    */
    private java.util.Date PayPeriodEndDate = new java.util.Date();
    public java.util.Date getPayPeriodEndDate() {
        return PayPeriodEndDate;
    }

    public void setPayPeriodEndDate(java.util.Date value) {
        PayPeriodEndDate = value;
    }

    /**
    * Pay Frequency
    */
    private PayFrequency payFrequency = PayFrequency.Weekly;
    public PayFrequency getPayFrequency() {
        return payFrequency;
    }

    public void setPayFrequency(PayFrequency value) {
        payFrequency = value;
    }

    /**
    * Annual Salary
    */
    private java.math.BigDecimal AnnualSalary;
    public java.math.BigDecimal getAnnualSalary() {
        return AnnualSalary;
    }

    public void setAnnualSalary(java.math.BigDecimal value) {
        AnnualSalary = value;
    }

    /**
    * Gross Pay
    */
    private java.math.BigDecimal GrossPay;
    public java.math.BigDecimal getGrossPay() {
        return GrossPay;
    }

    public void setGrossPay(java.math.BigDecimal value) {
        GrossPay = value;
    }

    /**
    * Net Pay
    */
    private java.math.BigDecimal NetPay;
    public java.math.BigDecimal getNetPay() {
        return NetPay;
    }

    public void setNetPay(java.math.BigDecimal value) {
        NetPay = value;
    }

    /**
    * Hourly Rate
    */
    private java.math.BigDecimal HourlyRate;
    public java.math.BigDecimal getHourlyRate() {
        return HourlyRate;
    }

    public void setHourlyRate(java.math.BigDecimal value) {
        HourlyRate = value;
    }

    /**
    * Cheque Number
    */
    private String ChequeNumber;
    public String getChequeNumber() {
        return ChequeNumber;
    }

    public void setChequeNumber(String value) {
        ChequeNumber = value;
    }

    /**
    * Date of Birth
    */
    private java.util.Date DateOfBirth = new java.util.Date();
    public java.util.Date getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(java.util.Date value) {
        DateOfBirth = value;
    }

    /**
    * Superannuation Fund Link
    */
    private SuperannuationFundLink SuperannuationFund;
    public SuperannuationFundLink getSuperannuationFund() {
        return SuperannuationFund;
    }

    public void setSuperannuationFund(SuperannuationFundLink value) {
        SuperannuationFund = value;
    }

    /**
    * Employment Classification Link
    */
    private PayrollEmploymentClassificationLink EmploymentClassification;
    public PayrollEmploymentClassificationLink getEmploymentClassification() {
        return EmploymentClassification;
    }

    public void setEmploymentClassification(PayrollEmploymentClassificationLink value) {
        EmploymentClassification = value;
    }

    /**
    * Payroll Advice Lines
    */
    private ArrayList<EmployeePayrollAdviceLine> Lines = new ArrayList<EmployeePayrollAdviceLine>();
    public ArrayList<EmployeePayrollAdviceLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<EmployeePayrollAdviceLine> value) {
        Lines = value;
    }

}


