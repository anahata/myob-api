//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
* Describes a photo resource
*/
public class Photo   
{
    /**
    * The location where this entity can be retrieved. (Read only)
    */
    private String URI;
    public String getURI() {
        return URI;
    }

    public void setString(String value) {
        URI = value;
    }

    /**
    * The binary data of the photo
    */
    private byte[] Data;
    public byte[] getData() {
        return Data;
    }

    public void setData(byte[] value) {
        Data = value;
    }

    /**
    * The mime type of the image e.g. image/png
    */
    private String MimeType;
    public String getMimeType() {
        return MimeType;
    }

    public void setMimeType(String value) {
        MimeType = value;
    }

    /**
    * A number that can be used for change control but does not preserve a date or a time. 
    * ONLY required for updating an existing photo. 
    * NOT required when creating a new photo.
    */
    private String RowVersion;
    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String value) {
        RowVersion = value;
    }

}


