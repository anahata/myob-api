//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



public enum Classification
{
    /**
    * Account classification
    * 
    * Accounts that have a DisplayID of 1-...
    */
    Asset,
    /**
    * Accounts that have a DisplayID of 2-...
    */
    Liability,
    /**
    * Accounts that have a DisplayID of 3-...
    */
    Equity,
    /**
    * Accounts that have a DisplayID of 4-...
    */
    Income,
    /**
    * Accounts that have a DisplayID of 5-...
    */
    CostOfSales,
    /**
    * Accounts that have a DisplayID of 6-...
    */
    Expense,
    /**
    * Accounts that have a DisplayID of 8-...
    */
    OtherIncome,
    /**
    * Accounts that have a DisplayID of 9-...
    */
    OtherExpense;
    
    public int getDisplayId() {
        return ordinal() + 1;
    }
}

