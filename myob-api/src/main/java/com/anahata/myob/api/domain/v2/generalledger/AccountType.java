//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



public enum AccountType
{
    /**
    * The Account Type
    * 
    * Is related to Classification
    * 
    * Has Asset classification
    */
    Bank,
    /**
    * Has Asset classification
    */
    AccountReceivable,
    /**
    * Has Asset classification
    */
    OtherCurrentAsset,
    /**
    * Has Asset classification
    */
    FixedAsset,
    /**
    * Has Asset classification
    */
    OtherAsset,
    /**
    * Has Liability classification
    */
    CreditCard,
    /**
    * Has Liability classification
    */
    AccountsPayable,
    /**
    * Has Liability classification
    */
    OtherCurrentLiability,
    /**
    * Has Liability classification
    */
    LongTermLiability,
    /**
    * Has Liability classification
    */
    OtherLiability,
    /**
    * Has Equity classification
    */
    Equity,
    /**
    * Has Income classification
    */
    Income,
    /**
    * Has CostOfSales classification
    */
    CostOfSales,
    /**
    * Has Expense classification
    */
    Expense,
    /**
    * Has OtherIncome classification
    */
    OtherIncome,
    /**
    * Has OtherExpense classification
    */
    OtherExpense
}

