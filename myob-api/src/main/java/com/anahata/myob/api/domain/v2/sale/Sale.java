//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.CardLink;
import com.anahata.myob.api.domain.v2.sale.SaleStatus;

/**
* Common class for sale Professional, Miscelleneous, Service and Item invoice resource
*/
public class Sale  extends BaseEntity 
{
    /**
    * Sales invoice number
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * Transaction date entry
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * Customer PO number
    */
    private String CustomerPurchaseOrderNumber;
    public String getCustomerPurchaseOrderNumber() {
        return CustomerPurchaseOrderNumber;
    }

    public void setCustomerPurchaseOrderNumber(String value) {
        CustomerPurchaseOrderNumber = value;
    }

    /**
    * The customer
    */
    private CardLink Customer;
    public CardLink getCustomer() {
        return Customer;
    }

    public void setCustomer(CardLink value) {
        Customer = value;
    }

    /**
    * Transaction promised date
    */
    private java.util.Date PromisedDate = new java.util.Date();
    public java.util.Date getPromisedDate() {
        return PromisedDate;
    }

    public void setPromisedDate(java.util.Date value) {
        PromisedDate = value;
    }

    /**
    * Amount still payable on the sales invoice
    */
    private java.math.BigDecimal BalanceDueAmount;
    public java.math.BigDecimal getBalanceDueAmount() {
        return BalanceDueAmount;
    }

    public void setBalanceDueAmount(java.math.BigDecimal value) {
        BalanceDueAmount = value;
    }

    /**
    * Invoice status
    */
    private SaleStatus Status = SaleStatus.Unknown;
    public SaleStatus getStatus() {
        return Status;
    }

    public void setStatus(SaleStatus value) {
        Status = value;
    }

}


