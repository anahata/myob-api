//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import java.util.ArrayList;

/**
* General Journal
*/
public class GeneralJournal  extends BaseEntity 
{
    /**
    * The display ID
    */
    private String DisplayID;
    public String getDisplayID() {
        return DisplayID;
    }

    public void setDisplayID(String value) {
        DisplayID = value;
    }

    /**
    * Date occurred
    */
    private java.util.Date DateOccurred = new java.util.Date();
    public java.util.Date getDateOccurred() {
        return DateOccurred;
    }

    public void setDateOccurred(java.util.Date value) {
        DateOccurred = value;
    }

    /**
    * Lines Amount is Tax Inclusive
    */
    private boolean IsTaxInclusive;
    public boolean getIsTaxInclusive() {
        return IsTaxInclusive;
    }

    public void setIsTaxInclusive(boolean value) {
        IsTaxInclusive = value;
    }

    /**
    * Journal Memo
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * Entry in this field is mandatory.
    */
    private GSTReportingMethod gstReportingMethod = GSTReportingMethod.Sale;
    public GSTReportingMethod getGSTReportingMethod() {
        return gstReportingMethod;
    }

    public void setGSTReportingMethod(GSTReportingMethod value) {
        gstReportingMethod = value;
    }

    /**
    * Year End Adjustment
    */
    private boolean IsYearEndAdjustment;
    public boolean getIsYearEndAdjustment() {
        return IsYearEndAdjustment;
    }

    public void setIsYearEndAdjustment(boolean value) {
        IsYearEndAdjustment = value;
    }

    /**
    * Category Id
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

    /**
    * Lines
    */
    private ArrayList<GeneralJournalLine> Lines = new ArrayList<GeneralJournalLine>();
    public ArrayList<GeneralJournalLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<GeneralJournalLine> value) {
        Lines = value;
    }

}


