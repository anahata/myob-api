//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.payrollCategory.PayrollCategoryLink;
import java.util.ArrayList;

/**
* Describes the EmployeePayrollDetails
*/
public class EmployeePayrollDetails  extends BaseEntity 
{
    /**
    * Employee
    */
    private EmployeeLink Employee;
    public EmployeeLink getEmployee() {
        return Employee;
    }

    public void setEmployee(EmployeeLink value) {
        Employee = value;
    }

    /**
    * Date of birth
    */
    private java.util.Date DateOfBirth = new java.util.Date();
    public java.util.Date getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(java.util.Date value) {
        DateOfBirth = value;
    }

    /**
    * Gender
    */
    private Gender gender = Gender.NotSet;
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender value) {
        gender = value;
    }

    /**
    * Start date of employment
    */
    private java.util.Date StartDate = new java.util.Date();
    public java.util.Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(java.util.Date value) {
        StartDate = value;
    }

    /**
    * Date of termination
    */
    private java.util.Date TerminationDate = new java.util.Date();
    public java.util.Date getTerminationDate() {
        return TerminationDate;
    }

    public void setTerminationDate(java.util.Date value) {
        TerminationDate = value;
    }

    /**
    * Employment Basis: Individual (Employee), LaborHire (e.g. Contractor), or Other
    */
    private EmploymentBasis employmentBasis = EmploymentBasis.Individual;
    public EmploymentBasis getEmploymentBasis() {
        return employmentBasis;
    }

    public void setEmploymentBasis(EmploymentBasis value) {
        employmentBasis = value;
    }

    /**
    * Employment category
    */
    private EmploymentCategory employmentCategory = EmploymentCategory.Permanent;
    public EmploymentCategory getEmploymentCategory() {
        return employmentCategory;
    }

    public void setEmploymentCategory(EmploymentCategory value) {
        employmentCategory = value;
    }

    /**
    * Employment Status
    */
    private EmploymentStatus employmentStatus = EmploymentStatus.FullTime;
    public EmploymentStatus getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(EmploymentStatus value) {
        employmentStatus = value;
    }

    /**
    * Employment Classification
    */
    private PayrollEmploymentClassificationLink EmploymentClassification;
    public PayrollEmploymentClassificationLink getEmploymentClassification() {
        return EmploymentClassification;
    }

    public void setEmploymentClassification(PayrollEmploymentClassificationLink value) {
        EmploymentClassification = value;
    }

    /**
    * Pay slip delivery options
    */
    private PaySlipDelivery paySlipDelivery = PaySlipDelivery.ToBePrinted;
    public PaySlipDelivery getPaySlipDelivery() {
        return paySlipDelivery;
    }

    public void setPaySlipDelivery(PaySlipDelivery value) {
        paySlipDelivery = value;
    }

    /**
    * Pay Slip Email
    */
    private String PaySlipEmail;
    public String getPaySlipEmail() {
        return PaySlipEmail;
    }

    public void setPaySlipEmail(String value) {
        PaySlipEmail = value;
    }

    /**
    * All information about the Employee's wage is here: Annyal Salary, Pay Basis, Wages Catgories...etc.
    */
    private EmployeeWageInfo Wage;
    public EmployeeWageInfo getWage() {
        return Wage;
    }

    public void setWage(EmployeeWageInfo value) {
        Wage = value;
    }

    /**
    * Details relating to the the employee's Supperannuation
    */
    private EmployeeSuperannuationInfo Superannuation;
    public EmployeeSuperannuationInfo getSuperannuation() {
        return Superannuation;
    }

    public void setSuperannuation(EmployeeSuperannuationInfo value) {
        Superannuation = value;
    }

    /**
    * Entitlements
    */
    private ArrayList<EmployeeEntitlementInfo> Entitlements = new ArrayList<EmployeeEntitlementInfo>();
    public ArrayList<EmployeeEntitlementInfo> getEntitlements() {
        return Entitlements;
    }

    public void setEntitlements(ArrayList<EmployeeEntitlementInfo> value) {
        Entitlements = value;
    }

    /**
    * Deduction Categories
    */
    private ArrayList<PayrollCategoryLink> Deductions = new ArrayList<PayrollCategoryLink>();
    public ArrayList<PayrollCategoryLink> getDeductions() {
        return Deductions;
    }

    public void setDeductions(ArrayList<PayrollCategoryLink> value) {
        Deductions = value;
    }

    /**
    * Expenses Categories
    */
    private ArrayList<PayrollCategoryLink> EmployerExpenses = new ArrayList<PayrollCategoryLink>();
    public ArrayList<PayrollCategoryLink> getEmployerExpenses() {
        return EmployerExpenses;
    }

    public void setEmployerExpenses(ArrayList<PayrollCategoryLink> value) {
        EmployerExpenses = value;
    }

    /**
    * All information about the Employee's tax is here: TaxFileNumber, TaxTable...etc.
    */
    private EmployeeTaxInfo Tax;
    public EmployeeTaxInfo getTax() {
        return Tax;
    }

    public void setTax(EmployeeTaxInfo value) {
        Tax = value;
    }

    /**
    * Time Billing Info
    */
    private EmployeeTimeBillingInfo TimeBilling;
    public EmployeeTimeBillingInfo getTimeBilling() {
        return TimeBilling;
    }

    public void setTimeBilling(EmployeeTimeBillingInfo value) {
        TimeBilling = value;
    }

}


