//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.inventory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.generalledger.CategoryLink;
import java.util.ArrayList;

/**
* InventoryAdjustment
*/
public class InventoryAdjustment  extends BaseEntity 
{
    /**
    * The inventory journal number
    */
    private String InventoryJournalNumber;
    public String getInventoryJournalNumber() {
        return InventoryJournalNumber;
    }

    public void setInventoryJournalNumber(String value) {
        InventoryJournalNumber = value;
    }

    /**
    * The date fo the adjustment
    */
    private String Date;
    public String getDate() {
        return Date;
    }

    public void setDate(String value) {
        Date = value;
    }

    /**
    * Is this a year end adjustment (period 13)
    */
    private boolean IsYearEndAdjustment;
    public boolean getIsYearEndAdjustment() {
        return IsYearEndAdjustment;
    }

    public void setIsYearEndAdjustment(boolean value) {
        IsYearEndAdjustment = value;
    }

    /**
    * A description of the adjustment
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * The adjustment entries
    */
    private ArrayList<InventoryAdjustmentLine> Lines = new ArrayList<InventoryAdjustmentLine>();
    public ArrayList<InventoryAdjustmentLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<InventoryAdjustmentLine> value) {
        Lines = value;
    }

    /**
    * The related category
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

}


