//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.payrollCategory.PayrollCategoryLink;
import java.util.ArrayList;

/**
* Employee Superannuation Info
*/
public class EmployeeSuperannuationInfo   
{
    /**
    * SuperannationFund link
    */
    private SuperannuationFundLink SuperannuationFund;
    public SuperannuationFundLink getSuperannuationFund() {
        return SuperannuationFund;
    }

    public void setSuperannuationFund(SuperannuationFundLink value) {
        SuperannuationFund = value;
    }

    /**
    * Employee Superannation Membership Number
    */
    private String EmployeeMembershipNumber;
    public String getEmployeeMembershipNumber() {
        return EmployeeMembershipNumber;
    }

    public void setEmployeeMembershipNumber(String value) {
        EmployeeMembershipNumber = value;
    }

    /**
    * Superannation Categories
    */
    private ArrayList<PayrollCategoryLink> SuperannuationCategories = new ArrayList<PayrollCategoryLink>();
    public ArrayList<PayrollCategoryLink> getSuperannuationCategories() {
        return SuperannuationCategories;
    }

    public void setSuperannuationCategories(ArrayList<PayrollCategoryLink> value) {
        SuperannuationCategories = value;
    }

}


