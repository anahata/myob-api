//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.payrollCategory.PayrollCategoryLink;
import java.util.ArrayList;

/**
* Employee Wage Info
*/
public class EmployeeWageInfo   
{
    /**
    * Pay Basis
    */
    private EmploymentPayBasis PayBasis = EmploymentPayBasis.Salary;
    public EmploymentPayBasis getPayBasis() {
        return PayBasis;
    }

    public void setPayBasis(EmploymentPayBasis value) {
        PayBasis = value;
    }

    /**
    * Annual Salary
    */
    private java.math.BigDecimal AnnualSalary;
    public java.math.BigDecimal getAnnualSalary() {
        return AnnualSalary;
    }

    public void setAnnualSalary(java.math.BigDecimal value) {
        AnnualSalary = value;
    }

    /**
    * Hourly Rate
    */
    private java.math.BigDecimal HourlyRate;
    public java.math.BigDecimal getHourlyRate() {
        return HourlyRate;
    }

    public void setHourlyRate(java.math.BigDecimal value) {
        HourlyRate = value;
    }

    /**
    * Pay Frequency
    */
    private PayFrequency payFrequency = PayFrequency.Weekly;
    public PayFrequency getPayFrequency() {
        return payFrequency;
    }

    public void setPayFrequency(PayFrequency value) {
        payFrequency = value;
    }

    /**
    * Hours In Weekly Pay Period
    */
    private java.math.BigDecimal HoursInWeeklyPayPeriod;
    public java.math.BigDecimal getHoursInWeeklyPayPeriod() {
        return HoursInWeeklyPayPeriod;
    }

    public void setHoursInWeeklyPayPeriod(java.math.BigDecimal value) {
        HoursInWeeklyPayPeriod = value;
    }

    /**
    * Wages Expense Account
    */
    private AccountLink WagesExpenseAccount;
    public AccountLink getWagesExpenseAccount() {
        return WagesExpenseAccount;
    }

    public void setWagesExpenseAccount(AccountLink value) {
        WagesExpenseAccount = value;
    }

    /**
    * Wage Categories
    */
    private ArrayList<PayrollCategoryLink> WageCategories = new ArrayList<PayrollCategoryLink>();
    public ArrayList<PayrollCategoryLink> getWageCategories() {
        return WageCategories;
    }

    public void setWageCategories(ArrayList<PayrollCategoryLink> value) {
        WageCategories = value;
    }

}


