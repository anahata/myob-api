//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.banking;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.CardLink;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.CategoryLink;
import com.anahata.myob.api.domain.v2.sale.DepositTo;
import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
* A receive money transaction entity
*/
public class ReceiveMoneyTxn  extends BaseEntity 
{
    /**
    * Deposit To "Account" OR "Undeposited Funds"
    */
    @SerializedName("DepositTo")
    private DepositTo depositTo = DepositTo.Account;
    public DepositTo getDepositTo() {
        return depositTo;
    }

    public void setDepositTo(DepositTo value) {
        depositTo = value;
    }

    /**
    * The account to DepositTo A
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * The Contact (Card) associated with the transaction
    */
    private CardLink Contact;
    public CardLink getContact() {
        return Contact;
    }

    public void setContact(CardLink value) {
        Contact = value;
    }

    /**
    * Receipt Number with the transcation
    */
    private String ReceiptNumber;
    public String getReceiptNumber() {
        return ReceiptNumber;
    }

    public void setReceiptNumber(String value) {
        ReceiptNumber = value;
    }

    /**
    * Transcation Receive Date
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * The Total Amount Received
    */
    private BigDecimal AmountReceived;
    public BigDecimal getAmountReceived() {
        return AmountReceived;
    }

    public void setAmountReceived(BigDecimal value) {
        AmountReceived = value;
    }

    /**
    * True indicates the transaction is set to tax inclusive.False indicates the transaction is not tax inclusive.
    */
    private boolean IsTaxInclusive;
    public boolean getIsTaxInclusive() {
        return IsTaxInclusive;
    }

    public void setIsTaxInclusive(boolean value) {
        IsTaxInclusive = value;
    }

    /**
    * Memo text describing the transaction.
    */
    private java.math.BigDecimal TotalTax;
    public java.math.BigDecimal getTotalTax() {
        return TotalTax;
    }

    public void setTotalTax(java.math.BigDecimal value) {
        TotalTax = value;
    }

    /**
    * The transaction payment method
    */
    private String PaymentMethod;
    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String value) {
        PaymentMethod = value;
    }

    /**
    * Memo text describing the transaction.
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * Transaction Lines
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

    /**
    * The Category associated with the transaction
    */
    private ArrayList<ReceiveMoneyTxnLine> Lines = new ArrayList<ReceiveMoneyTxnLine>();
    public ArrayList<ReceiveMoneyTxnLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<ReceiveMoneyTxnLine> value) {
        Lines = value;
    }

}


