/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.transport;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.anahata.myob.api.MyobEndPoint;
import com.anahata.myob.api.service.exception.MyobException;
import java.net.URI;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import static com.anahata.myob.api.transport.AbstractMyobTransport.*;

/**
 *
 * @author pablo
 */
@Slf4j
public class HttpClientMyobTransport extends AbstractMyobTransport {
    private static org.apache.http.client.HttpClient httpClient = HttpClientBuilder.create().build();

    @Override
    public String sendReceiveURL(MyobEndPoint endpoint, URI url, String method, String payLoad) throws Exception {
        System.out.println("URL:::"+url.toString());;
        
        HttpUriRequest request = new HttpGet(url);
        if (method != null) {
            switch (method) {
                case "POST":
                    request = new HttpPost(url);
                    break;
                case "PUT":
                    request = new HttpPut(url);
                    break;
                case "DELETE":
                    request = new HttpDelete(url);
                    break;
            }
        }

        request.addHeader(MYOB_API_VERSION_HEADER, getApiVersion());

        if (endpoint.getCredentials() != null) {
            request.addHeader(MYOB_API_CFTOKEN_HEADER, makeCFTokenHeader(endpoint.getCredentials()));
        }

        if (endpoint.getOauthAccessToken() != null) {
            request.addHeader(MYOB_API_AUTHORIZATION_HEADER,
                    MYOB_API_AUTHORIZATION_HEADER_BEARER + endpoint.getOauthAccessToken().getAccessToken());
            request.addHeader(MYOB_API_KEY_HEADER, endpoint.getOauthAccessToken().getPlugin().getKey());
        }

        request.addHeader("Content-Type", "text/json");

        if (payLoad != null && request instanceof HttpEntityEnclosingRequestBase) {
            ((HttpEntityEnclosingRequestBase)request).setEntity(new StringEntity(payLoad));
        }

        org.apache.http.client.ResponseHandler<String> responseHandler = new BasicResponseHandler();
        
        try {
            String ret = httpClient.execute(request, responseHandler);
            log.debug("Got response from MYOB {} " + url, ret);
            return ret;
        } catch (HttpResponseException e) {
            log.debug("Exception caught calling Myob API {}, rethrowing as MyobServiceException", e);
            throw new MyobException(e.getStatusCode(), e.getMessage(), e);
        }

    }
}
