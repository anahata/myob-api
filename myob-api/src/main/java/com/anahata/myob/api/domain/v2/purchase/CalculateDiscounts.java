//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.purchase;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;

/**
* Describe the Purchase/SupplierPayment/CalculateDiscounts request
*/
public class CalculateDiscounts   
{
    /**
    * Collection of item required to do discount calculation
    */
    private ArrayList<CalculateDiscountsItem> Items = new ArrayList<CalculateDiscountsItem>();
    public ArrayList<CalculateDiscountsItem> getItems() {
        return Items;
    }

    public void setItems(ArrayList<CalculateDiscountsItem> value) {
        Items = value;
    }

}


