//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.company;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import com.anahata.myob.api.domain.v2.Terms;

/**
* Sales' terms preferences
*/
public class CompanySalesPreferencesTerms  extends Terms 
{
    /**
    * % Monthly charge for late payment
    */
    private java.math.BigDecimal MonthlyChargeForLatePayment;
    public java.math.BigDecimal getMonthlyChargeForLatePayment() {
        return MonthlyChargeForLatePayment;
    }

    public void setMonthlyChargeForLatePayment(java.math.BigDecimal value) {
        MonthlyChargeForLatePayment = value;
    }

    /**
    * BaseSellingPriceLevelALevelBLevelCLevelDLevelELevelF
    */
    private PriceLevel priceLevel = PriceLevel.BaseSellingPrice;
    public PriceLevel getPriceLevel() {
        return priceLevel;
    }

    public void setPriceLevel(PriceLevel value) {
        priceLevel = value;
    }

    /**
    * Tax code of sales
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

    /**
    * Use customer's tax code instead of default tax code
    */
    private boolean UseCustomerTaxCode;
    public boolean getUseCustomerTaxCode() {
        return UseCustomerTaxCode;
    }

    public void setUseCustomerTaxCode(boolean value) {
        UseCustomerTaxCode = value;
    }

    /**
    * Tax code for sale freight
    */
    private TaxCodeLink FreightTaxCode;
    public TaxCodeLink getFreightTaxCode() {
        return FreightTaxCode;
    }

    public void setFreightTaxCode(TaxCodeLink value) {
        FreightTaxCode = value;
    }

    /**
    * Credit limit of a sale
    */
    private java.math.BigDecimal CreditLimit;
    public java.math.BigDecimal getCreditLimit() {
        return CreditLimit;
    }

    public void setCreditLimit(java.math.BigDecimal value) {
        CreditLimit = value;
    }

}


