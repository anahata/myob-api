//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.CustomerLink;
import java.util.ArrayList;

/**
* Describe the Sale/CreditSettlement resource
*/
public class CreditSettlement  extends BaseEntity 
{
    /**
    * A reference to the invoice from where the credit note came from
    */
    private InvoiceLink CreditFromInvoice;
    public InvoiceLink getCreditFromInvoice() {
        return CreditFromInvoice;
    }

    public void setCreditFromInvoice(InvoiceLink value) {
        CreditFromInvoice = value;
    }

    /**
    * The customer related to this creditnote
    */
    private CustomerLink Customer;
    public CustomerLink getCustomer() {
        return Customer;
    }

    public void setCustomer(CustomerLink value) {
        Customer = value;
    }

    /**
    * The number of the credit note
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * The date the credit note was applied
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * The amount applied
    */
    private java.math.BigDecimal CreditAmount;
    public java.math.BigDecimal getCreditAmount() {
        return CreditAmount;
    }

    public void setCreditAmount(java.math.BigDecimal value) {
        CreditAmount = value;
    }

    /**
    * Extra details
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * The invoices and orders to which this settlement was applied
    */
    private ArrayList<CreditSettlementLine> Lines = new ArrayList<CreditSettlementLine>();
    public ArrayList<CreditSettlementLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<CreditSettlementLine> value) {
        Lines = value;
    }

}


