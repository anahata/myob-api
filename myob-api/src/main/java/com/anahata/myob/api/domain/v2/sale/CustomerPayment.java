//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.CardLink;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import java.util.ArrayList;

/**
* Describe the CustomerPayment resource
*/
public class CustomerPayment  extends BaseEntity 
{
    /**
    * Initialise
    */
    public CustomerPayment() {
        
    }

    /**
    * If allocating a banking account for the payment specify AccountIf using undeposited funds specify UndepositedFunds
    */
    private DepositTo DepositTo = com.anahata.myob.api.domain.v2.sale.DepositTo.Account;
    public DepositTo getDepositTo() {
        return DepositTo;
    }

    public void setDepositTo(DepositTo value) {
        DepositTo = value;
    }

    /**
    * The account to deposit to (only need UID)
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * Pay to customer
    */
    private CardLink Customer;
    public CardLink getCustomer() {
        return Customer;
    }

    public void setCustomer(CardLink value) {
        Customer = value;
    }

    /**
    * ID No of payment transaction
    */
    private String ReceiptNumber;
    public String getReceiptNumber() {
        return ReceiptNumber;
    }

    public void setReceiptNumber(String value) {
        ReceiptNumber = value;
    }

    /**
    * Transaction date entry, format YYYY-MM-DD HH:MM:SS
    * { 'Date': '2013-08-11 13:33:02' }
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * Total of all amounts applicable to the sale invoice.
    */
    private java.math.BigDecimal AmountReceived;
    public java.math.BigDecimal getAmountReceived() {
        return AmountReceived;
    }

    public void setAmountReceived(java.math.BigDecimal value) {
        AmountReceived = value;
    }

    /**
    * Payment method text.
    */
    private String PaymentMethod;
    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String value) {
        PaymentMethod = value;
    }

    /**
    * Memo text of the customer payment entry.
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * Collection of invoice to pay
    */
    private ArrayList<CustomerPaymentLine> Invoices = new ArrayList<CustomerPaymentLine>();
    public ArrayList<CustomerPaymentLine> getInvoices() {
        return Invoices;
    }

    public void setInvoices(ArrayList<CustomerPaymentLine> value) {
        Invoices = value;
    }

}


