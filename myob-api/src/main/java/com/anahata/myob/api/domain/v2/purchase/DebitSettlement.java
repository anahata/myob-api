//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.purchase;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.SupplierLink;
import java.util.ArrayList;

/**
* Describe the Purchase/DebitSettlement resource
*/
public class DebitSettlement  extends BaseEntity 
{
    /**
    * A reference to the bill from where the debit note came from
    */
    private BillLink DebitFromBill;
    public BillLink getDebitFromBill() {
        return DebitFromBill;
    }

    public void setDebitFromBill(BillLink value) {
        DebitFromBill = value;
    }

    /**
    * The supplier related to this debit note
    */
    private SupplierLink Supplier;
    public SupplierLink getSupplier() {
        return Supplier;
    }

    public void setSupplier(SupplierLink value) {
        Supplier = value;
    }

    /**
    * The number of the debit note
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * The date the debit note was applied
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * The amount applied
    */
    private java.math.BigDecimal DebitAmount;
    public java.math.BigDecimal getDebitAmount() {
        return DebitAmount;
    }

    public void setDebitAmount(java.math.BigDecimal value) {
        DebitAmount = value;
    }

    /**
    * Extra details
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * The bills and orders to which this settlement was applied
    */
    private ArrayList<DebitSettlementLine> Lines = new ArrayList<DebitSettlementLine>();
    public ArrayList<DebitSettlementLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<DebitSettlementLine> value) {
        Lines = value;
    }

}


