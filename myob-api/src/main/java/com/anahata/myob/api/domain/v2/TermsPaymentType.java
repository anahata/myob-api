//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



public enum TermsPaymentType
{
    /**
    * Describes the different term payment types
    * 
    * Cash on delivery
    */
    CashOnDelivery,
    /**
    * Pre-paid
    */
    Prepaid,
    /**
    * In a given number of days
    */
    InAGivenNumberOfDays,
    /**
    * on a particular day of the month i.e. 8th
    */
    OnADayOfTheMonth,
    /**
    * Number of days after the end of the month
    */
    NumberOfDaysAfterEOM,
    /**
    * The day of the month after the end of the month
    */
    DayOfMonthAfterEOM
}

